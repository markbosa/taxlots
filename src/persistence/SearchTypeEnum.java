package persistence;

/**
 * An enumeration to identify supported search types
 * @author Mark
 *
 */

public interface SearchTypeEnum {
	public int FindByOpenTaxLotId = 1;
	public int FindByOrderId = 2;
	public int FindByClosedTaxLotId = 3;
	public int FindByCorporateAction = 4;
	public int FindByAccountIdAndSymbol = 5;		
}
