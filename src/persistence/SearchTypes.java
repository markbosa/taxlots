package persistence;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

/**
 * SearchTypes - A collection of parameterized searches on taxLot data
 * @author markbosa
 * Uses interface SearchTypesInf.java to define search types
 */
public class SearchTypes extends MongoDBUtils {
	private MongoDBUtils mongoConn = null;
	public int searchId = 0;

	/**
	 * Getter and Setter Methods
	 */
	public int getSearchId() {
		return searchId;
	}

	/**
	 * setSearchId() - Validate search type request and only set value if
	 * 					it is a valid search type.
	 * @param( newSearchId ) - MUST be one of the types defined in 
	 * 							SearchTypeEnum.java 
	 */	
	public void setSearchId(int newSearchId) {
		switch(newSearchId) {
		case SearchTypeEnum.FindByAccountIdAndSymbol :
		case SearchTypeEnum.FindByClosedTaxLotId:
		case SearchTypeEnum.FindByOpenTaxLotId:
		case SearchTypeEnum.FindByOrderId:
		case SearchTypeEnum.FindByCorporateAction:  
			this.searchId = newSearchId;
			break;
		default:
			this.searchId = 0;
			break;
		}
	}

	public SearchTypes(MongoDBUtils mongoConnection) {
		this.searchId = 0;
		this.mongoConn = mongoConnection;
		// TODO Auto-generated constructor stub
	}
	/**
	 * SearchByDataset() - Handles the SearchType.FindByDataset
	 * @param dataset
	 */
	protected void SearchByDataset(String dataset) {
		/**
		 * Now query the data with find() 
		 */		
		String datasetQ = "{ dataset : \"" + dataset + "\" }";
		DBObject query = (DBObject) JSON.parse(datasetQ) ;  //"{name:{$exists:true}}");

//		DBCursor myCursor = this.mongoConn.getCollection().find( query  );
//		while(myCursor.hasNext()) {
//			System.out.println(myCursor.next());
//		}
	} 
	/**
	 * SearchByDTS() - Handles the SearchType.FindByDTS
	 * @param dts - The date-time string
	 */
	protected void SearchByDTS(String dts) {
		/**
		 * Now query the data with findOne()
		 */		
		String dtsQ = "{ dts : \"" + dts + "\" }";
		DBObject query = (DBObject) JSON.parse(dtsQ) ;  //"{name:{$exists:true}}");

//		DBCursor myCursor = this.mongoConn.getCollection().find( query  );
//		while(myCursor.hasNext()) {
//			System.out.println(myCursor.next());
//		}
	} 
	/**
	 * SearchByFrameID() - Handles the SearchType.findByFrameID
	 * @param frameId
	 */
	protected void SearchByFrameID(String frameId) {
		/**
		 * Now query the data with findOne()
		 */		
		String frameIdQ = "{ frameId : \"" + frameId + "\" }";
		DBObject query = (DBObject) JSON.parse(frameIdQ) ;  //"{name:{$exists:true}}");

//		DBCursor myCursor = this.mongoConn.getCollection().find( query  );
//		while(myCursor.hasNext()) {
//			System.out.println(myCursor.next());
//		}
	} 
	/**
	 * SearchByROI() - Handles the SearchType.FindByROI
	 * @param frameId
	 */
	protected void SearchByROI(String roi) {
		/**
		 * Now query the data with findOne()
		 */		
		String roiQ = "{ roiNum : \"" + roi + "\" }";
		DBObject query = (DBObject) JSON.parse(roiQ) ;  //"{name:{$exists:true}}");

//		DBCursor myCursor = this.mongoConn.getCollection().find( query  );
//		while(myCursor.hasNext()) {
//			System.out.println(myCursor.next());
//		}
	} 

	/**
	 * SearchByClassification() - Handles the SearchType.FindByClassification
	 * @param tag value pair [classification : value] where
	 *     Classification :  derived from the file system directory ROOT/classification
	 *              Value : One of the biology types such as Jellies or Fish 
	 */
	protected void SearchByClassification(String classification) {
		/**
		 * Now query the data with findOne()
		 */		
		String classificationQ = "{ Classification : \"" + classification + "\" }";
		DBObject query = (DBObject) JSON.parse(classificationQ) ;  //"{name:{$exists:true}}");
// NOTE: Removed collectionName from mongoConn..... This class is not used... stub out for now.
//		DBCursor myCursor = this.mongoConn.getCollection().find( query  );
//		while(myCursor.hasNext()) {
//			System.out.println(myCursor.next());
//		}
	}
}
