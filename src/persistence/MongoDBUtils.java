package persistence;



import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

/**
 * MongoDBUtils() - The highest level class to encapsulate the logic required
 * to create one or more connections to a MongoDB database.
 * 
 * Class Hierarchy Notes:
 *     This base class is a superclass for lower level MongoDB CRUD operations
 *     Possible future enhancements include adding a connection pool   
 * 
 * @author markbosa
 *
 */
public class MongoDBUtils {
	/**
	 * Define default MongoDB host and database name configuration parameters
	 * This class is used in conjunction with another class that defines
	 * the MongoDB Collection as this connection is re-usable 
	 */
	private int portNum = 27017;
	private String hostName = "localhost";
	private String myUserName = "username";
	private String myPassword = "password";
	private String dbName = "";
	private DB db = null;
	private MongoClient mongoClient;

	
	public MongoDBUtils() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * createMongoClient() 
	 * 	1) Default : Uses the compiled default values for:
	 *      a)port number
	 *      b)host-name 
	 *      c)Database names 
	 *  2) To Override: Set hostName, portNum and dbName via setter methods
	 */
	public boolean createMongoClient() {
		boolean rc = false;

		try {
			this.setMongoClient( new MongoClient( this.hostName , this.portNum ));
			this.setDb(this.getMongoClient().getDB( this.getDbName()));

			/**
			 * Set the write concern to SAFE as we do not want "fire-and-forget"
			 */
			this.getMongoClient().setWriteConcern(WriteConcern.SAFE);  // formerly  .ACKNOWLEDGED);
//			this.setCollection(db.getCollection(this.getCollectionName()));
			rc = true;
		} catch (UnknownHostException e) {
			this.setMongoClient(null);
			this.setDb(null);
//		this.setCollection(null);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rc;
	}

	/************************* Getter and Setter Methods *********************/
	/**
	 * @return the portNum
	 */
	public int getPortNum() {
		return portNum;
	}

	/**
	 * @param portNum the portNum to set
	 */
	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	/**
	 * @return the myUserName
	 */
	public String getMyUserName() {
		return myUserName;
	}

	/**
	 * @param myUserName the myUserName to set
	 */
	public void setMyUserName(String myUserName) {
		this.myUserName = myUserName;
	}

	/**
	 * @return the myPassword
	 */
	public String getMyPassword() {
		return myPassword;
	}

	/**
	 * @param myPassword the myPassword to set
	 */
	public void setMyPassword(String myPassword) {
		this.myPassword = myPassword;
	}

	/**
	 * @return the database name
	 */
	public DB getDb() {
		return db;
	}

	/**
	 * @param db the database name to set for the MongoDB "use db" command
	 */
	public void setDb(DB db) {
		this.db = db;
	}

	/**
	 * @return the Mongo Client instance
	 */
	public MongoClient getMongoClient() {
		return mongoClient;
	}
	/**
	 *  set the MongoClient instance with an initialized value
	 */
	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}

	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}
	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}	

	/********************** Main() Entry Point *******************************/
	/**
	 * Test the MongoDB connection
	 * @param args
	 */
	public static void main(String[] args) {
		MongoDBUtils mDbu = new MongoDBUtils();

		/**
		 * Set (or over-ride) the Mongo-Driver connection parameters 
		 */
		mDbu.setPortNum(27017);
		mDbu.setHostName("10.27.2.221");
		mDbu.setDbName("taxLots");

		/**
		 * Now instantiate a single MongoDB client connection
		 * This has the side-effect of setting the two key instance variables
		 * "mongoClient" and the "this.connection" used by Search Type Queries 
		 */
		Boolean mrc = mDbu.createMongoClient();
		System.out.println("main()--> " +
				((mrc == true) ? " Successfully Connected" : "Failed to Connect ")	
				+ " to MongoDB Database:: " + mDbu.getDbName() 
				+ " on Host:: " + mDbu.getHostName() + " on Port:: " + mDbu.portNum);
	}
}

