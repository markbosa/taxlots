package persistence;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import calcEngine.TaxLot;

public class DaoSqlTaxLot {
	SqlServerClient ssClient = null;

	public DaoSqlTaxLot(SqlServerClient sqlServerClient) {
		this.ssClient = sqlServerClient;
	}


	public void insertTaxLot(TaxLot tl) {
		// create our java jdbc statement

		Statement statement;
		try {
			statement = ssClient.getConn().createStatement();


			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
			Calendar cal = Calendar.getInstance();  
			System.out.println(dateFormat.format(cal.getTime()));

			String Values =	"'"+ tl.getLotId() + "',"+ 
					"'"+ tl.getAccount() + "',"+ 
					"'"+ tl.getOpeningTicket().getHostOrderId() + "',"+
					"'"+ tl.getOpeningTicket().getOpeningOpenOrd().getTradeId() + "',"+
					"'"+ tl.getSecurityId() + "',"+
					tl.getLotDate() +  ","+
					tl.getLotOriginalQty() +  ","+
					tl.getLotOriginalCost() + ","+
					tl.getLotUnitCost() + ","+
					tl.getPreviousDayPosition() + ","+
					tl.getOpeningTicket().getOpeningOpenOrd().getQuantity() + ","+
					tl.getBoughtQty() +  ","+
					tl.getCurrentClosingOrder().getBalance() + ","+
					tl.getPrice() + ","+
					tl.getMarketValue() + ","+
					tl.getOpenOrder() +  ","+
					"'"+ tl.getOpeningTicket().getOpeningOpenOrd().getCorporateActionType() + "',"+
					"'"+ tl.getOpeningTicket().getOpeningOpenOrd().getCurrency() + "',"+
					tl.getOpeningTicket().getOpeningOpenOrd().getBalance() +  ","+
					"'ProductDescription Filler'";


			System.out.println("DaoOpenTaxLot::insertTaxLot: Values = [" + Values + "]");

			String SQL = "INSERT INTO [LiquidView].[dbo].[OpenTaxLots]" +
					"( LotID, AccountId,OrderId, OpeningTradeId, SeccurityId, LotDate, LongTermShortTerm, LotOriginalQty," +
					"LotOriginalCost, LotUnitCost, PrevDayPosition,TodayPosition, Bought, Sold, Price," + 
					"MarketValue, Buy, CorporateActionType, Currency, Balance, ProductDescription)" +
					" VALUES (" + Values + ")";		

			System.out.println("DaoOpenTaxLot::insertTaxLot: SQL = [" + SQL + "]");
			statement.executeUpdate(SQL);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}
}
