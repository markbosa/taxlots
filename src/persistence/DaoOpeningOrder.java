package persistence;

import java.util.Date;
import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import orders.OpeningOrder;
import orders.OpeningOrderTicket;
import orders.Transaction.TaxLotStatus;

public class DaoOpeningOrder extends Collection {
	private String collName = "openingOrder";

	public DaoOpeningOrder(OpeningOrderTicket openingOrderTicket, MongoDBUtils mongoUtils) {
		super(mongoUtils);
		this.setDBCollection(mongoUtils.getDb().getCollection(this.collName));
	}
	
	public void insertOpeningOrder(OpeningOrder oo ) {	
		/**
		 * Create a BSON formatted document with image and text data
		 */
		DBObject bsonDoc = new BasicDBObject();
		
		/**
		 * Use a common method to populate bsonDoc for consistencies sake
		 */
		transferPutObject(bsonDoc, oo);
				
		System.out.println("DaoOpeningOrder::insertOpenOrder: TradeId: " +
							oo.getTradeId() + " Symbol: " + 
							oo.getSymbol() + " Balance: " +
							oo.getBalance());

		this.getDBCollection().insert(bsonDoc);
	}

	/**
	 * Insert a new document with opening order 
	 * @param openeOrd
	 */
	public boolean updateOpeningOrder(OpeningOrder oo) {
	
		/**
		 * Create a BSON formatted document with image and text data
		 */
		DBObject bsonDoc = new BasicDBObject();
		
		transferPutObject(bsonDoc, oo);
				
		System.out.println("DaoOpeningOrder::updateOpeningOrder: TradeId: " +
							oo.getTradeId() + " Symbol: " + 
							oo.getSymbol() + " Balance: " +
							oo.getBalance());

		/**
		 * Find the record to update with this query
		 */
		BasicDBObject query = new BasicDBObject();
		query.put("HostOrderId", 	oo.getHostOrderId());
		query.put("TradeId", 		oo.getTradeId());						
		
		WriteResult result = this.getDBCollection().update(query, bsonDoc);
		
		if(result.isUpdateOfExisting() == true )
			return true;
		else {
			System.out.println(
					"DaoOpeningOrder:: Error updating TradeId[" + oo.getTradeId() +
					"] Error= " + result.toString() + 
					" Suggest stop processing - check SequenceManger collection");
			return false;
		}
	}
	
	/**
	 * transferObject() - A helper method to enforce a single place to define
	 * the content of the OpeningOrder schema for insert and update methods.
	 * @param bsonDoc - A DBObject 
	 * @param oo - Opening Order
	 */
	void transferPutObject(DBObject bsonDoc, OpeningOrder oo) {
		
		bsonDoc.put("Status",          	oo.status.toString());
		bsonDoc.put("HostOrderId",		oo.getHostOrderId());
		bsonDoc.put("AccountId", 		oo.getAccountId());
		bsonDoc.put("TradeId",			oo.getTradeId());
		bsonDoc.put("ExchangeMIC",		oo.getExchangeMIC());
		bsonDoc.put("MarketMIC",		oo.getMarketMIC());
		bsonDoc.put("AssetType", 		oo.getAssetType());
		bsonDoc.put("ProductDesc", 		oo.getTradeText());
		if(oo.getCorporateActionType() != null)
			bsonDoc.put("CorpActionType",	oo.getCorporateActionType() );
		bsonDoc.put("Date",		 		oo.getDate());
		bsonDoc.put("Quantity", 		oo.getQuantity());
		bsonDoc.put("Balance",			oo.getBalance());
		bsonDoc.put("Price",			oo.getPrice());
		bsonDoc.put("OrderValue",		oo.getOrderValue());
		bsonDoc.put("Currency", 		oo.getCurrency());
		bsonDoc.put("Symbol",			oo.getSymbol());
		bsonDoc.put("ISIN",				oo.getIsin());
	}
	
	
	/**
	 * transferGetObject() - Used as a common method to transfer the whole
	 * document contents for various find() operations.
	 */
	void transferGetObject(DBObject doc, OpeningOrder oo) {
		
		oo.status = TaxLotStatus.valueOf((String) doc.get("Status"));
		oo.setHostOrderId((String)		doc.get("HostOrderId"));
		oo.setAccountId((String)		doc.get("AccountId"));
		oo.setTradeId((String)			doc.get("TradeId"));
		oo.setExchangeMIC((String)		doc.get("ExchangeMIC"));
		oo.setMarketMIC((String)		doc.get("MarketMIC"));
		oo.setAssetType((String)		doc.get("AssetType"));
		oo.setTradeText((String)		doc.get("ProductDesc"));
		oo.setCorporateActionType((String)doc.get("CorpActionType"));
		oo.setDate((Date)				doc.get("Date"));
		oo.setQuantity((int)			doc.get("Quantity"));
		oo.setBalance((int)				doc.get("Balance"));
		oo.setPrice((double)			doc.get("Price"));
		oo.setOrderValue((double)		doc.get("OrderValue"));
		oo.setCurrency((String)			doc.get("Currency"));
		oo.setSymbol((String)			doc.get("Symbol"));
		oo.setIsin((String) 			doc.get("ISIN"));
	}
	
	
	/**
	 * Custom method to find all OpeningOrders for a specific Account ID
	 * All items that are retrieved are added to the OpeningOrderTicket
	 * 
	 * @param accountId
	 * @param symbol
	 * @param openingOrderTicket 
	 * @return A Linked List of Open Orders for this account
	 */
	public LinkedList<OpeningOrder> findByAccountIdAndSymbol(
					String accountId, 
					String symbol,
					OpeningOrderTicket openingOrderTicket) {
		
		LinkedList<OpeningOrder> retList = new LinkedList<OpeningOrder>();
				
		BasicDBObject whereClause = new BasicDBObject();
		whereClause.put("AccountId", accountId);
		whereClause.put("Symbol", symbol);
		
//		BasicDBObject fields = new BasicDBObject();
//		fields.put("employeeId", 1);

		/**
		 * Now query the data with ...find(whereClause)
		 */
		DBCursor cursor = this.getDBCollection().find(whereClause); //, fields);

		try {
			while (cursor.hasNext()) {
				DBObject doc = (DBObject)cursor.next();

				OpeningOrder oo = new OpeningOrder();

				transferGetObject(doc, oo);
				retList.add(oo);
//				System.out.println(cursor.next());
			}
		} finally {
			cursor.close();
		}			
		return retList;
	}
}
