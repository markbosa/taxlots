package persistence;
//package com.gmanalytics.symbology.persistence.impl.mongoDriver.MongoDba;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * MongoConnection - Manages the Mongo database has 1-N collections pattern
 * <p/>
 * A transparent abstract class that requires each MongoDB collection to set
 * its own collection name and connect with the inherited MongoDB database
 * MongoDB connection parameters are specified in MongoDUtils
 * <p/>
 * This is an abstract class as it allows for multiple connections to be
 * created from a base MongoDB connection class
 *
 * @author Mark Bosakowski
 */
public abstract class Collection {
    protected MongoDBUtils mongoDriverUtil = null;
    private DBCollection collection = null;

    Collection() {
    	if(this.mongoDriverUtil == null) {
    		
    	}
    }

    /**
     * Constructor MUST have a valid mongo connection
     *
     * @param mongoDbu
     */
    protected Collection(MongoDBUtils mongoDbu) {
        this.setMongoDbUtil(mongoDbu);
    }

    /**
     * Getter and Setter methods
     */
    public MongoDBUtils getMongoDbUtil() {
        return mongoDriverUtil;
    }

    public void setMongoDbUtil(MongoDBUtils mongoDbUtil) {
        this.mongoDriverUtil = mongoDbUtil;
    }

    /**
     * @return the collection
     */
    public DBCollection getDBCollection() {

        return collection;
    }

    /**
     * @param collection the collection to set
     */
    public void setDBCollection(DBCollection collection) {
        this.collection = collection;
    }

    /**
     * General purpose method to find documents in this collection
     * Not sure if this is ever used....
     * @return
     */
	public Object find() {
		System.out.println("Collection::find() WIP");
		
//		BasicDBObject filter = new BasicDBObject();
//		filter.put("_id", );
		
		DBObject dbo = this.getDBCollection().findOne();  // filter);
		return dbo;
	}

	  /**
     * General purpose method to find documents in this collection
     * @return
     */
	public Object find(Object id) {
		System.out.println("Collection::find(Object id) WIP");
		
		BasicDBObject filter = new BasicDBObject();
		filter.put("_id", id);
		
		DBObject dbo = this.getDBCollection().findOne(filter);
		return dbo;
	}
	/**
	 * findALL() generic's based method requiring caller to specify which
	 *           "type" of list items are to be retrieved
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> findAll( Class<T> type) {

		List<T> retList = new ArrayList<T>();

		DBCursor cursor = this.getDBCollection().find();
		try {
			   while(cursor.hasNext()) {
				   
					if(type.isAssignableFrom(cursor.getClass())) {
					    retList.add((T) cursor.next());
					}
			       // System.out.println(cursor.next());
			   }
			} finally {
			   cursor.close();
			}
		return (List<T>) retList;
	}
	/**
	 * getCount() generic's based method to get the count of a collection
	 */
	public int getCount() {
		int cnt = this.getDBCollection().find().count();
		System.out.println("Collection::count() = WIP" + cnt);
		return cnt;
	}
	
}

