package persistence;

import java.util.LinkedList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

import orders.ClosingOrder;
import orders.ClosingOrderTicket;
import orders.Transaction.TaxLotStatus;

public class DaoClosingOrder extends Collection {
	private ClosingOrderTicket cot = null;
	private String collName = "closingOrder";

	public DaoClosingOrder(ClosingOrderTicket closingOrderTicket, MongoDBUtils mongoUtils) {
		super(mongoUtils);
		this.setDBCollection(mongoUtils.getDb().getCollection(this.collName));
	}

	public DaoClosingOrder(ClosingOrderTicket closingOrderTicket) {
		this.setCot(closingOrderTicket);
	}

	/**
	 * Insert a new document with closing order 
	 * @param closeOrd
	 */
	public boolean updateClosingOrder(ClosingOrder co) {
	
		/**
		 * Create a BSON formatted document with image and text data
		 */
		DBObject bsonDoc = new BasicDBObject();
		
		transferObject(bsonDoc, co);
				
//		System.out.println("DaoClosedTaxLot::updateClosingOrder: TradeId: " +
//							co.getTradeId() + " Symbol: " + 
//							co.getInstrument() + " Balance: " +
//							co.getBalance());

		/**
		 * Find the record to update with this query
		 */
		BasicDBObject query = new BasicDBObject();
		query.put("HostOrderId", 	co.getHostOrderId());
		query.put("TradeId", 		co.getTradeId());	
					
		System.out.println("DaoClosedTaxLot::updateClosingOrder: " + co.getHostOrderId() + " : " + 
							co.getSymbol() + " : " +
							co.getBalance());

		WriteResult result = this.getDBCollection().update(query, bsonDoc);
		
		if(result.isUpdateOfExisting() == true )
			return true;
		else {
			System.out.println(
					"DaoClosingOrder:: Error updating TradeId[" + co.getTradeId() +
					"] Error= " + result.toString() + 
					" Suggest stop processing - check SequenceManger collection");
			return false;
		}

	}

	/**
	 * Update a new document with closing order 
	 * @param closeOrd
	 */
	public void insertClosingOrder(ClosingOrder co) {
	
		/**
		 * Create a BSON formatted document with image and text data
		 */
		DBObject bsonDoc = new BasicDBObject();
		
		transferObject(bsonDoc, co);
				
//		System.out.println("DaoClosedTaxLot::insertClosingOrder: TradeId: " +
//							co.getTradeId() + " Symbol: " + 
//							co.getInstrument() + " Balance: " +
//							co.getBalance());

		this.getDBCollection().insert(bsonDoc);
	}
	
	/**
	 * transferObject() - A helper method to enforce a single place to define
	 * the content of the ClosingOrder schema for CRUD insert and update.
	 * @param bsonDoc - A DBObject 
	 * @param oo - Closing Order
	 */
	void transferObject(DBObject bsonDoc, ClosingOrder co) {
		bsonDoc.put("Status",		    co.status.toString());
		bsonDoc.put("HostOrderId",		co.getHostOrderId());
		bsonDoc.put("AccountId", 		co.getAccountId() );
		bsonDoc.put("TradeId",			co.getTradeId());
		bsonDoc.put("ExchangeMIC",		co.getExchangeMIC());
		bsonDoc.put("MarketMIC",		co.getMarketMIC());
		bsonDoc.put("AssetType", 		co.getAssetType());
		bsonDoc.put("ProductDesc", 		co.getTradeText());
		if(co.getCorporateActionType() != null)
			bsonDoc.put("CorpActionType",	co.getCorporateActionType() );
		bsonDoc.put("Date",		 		co.getDate());
		bsonDoc.put("Quantity", 		co.getQuantity());
		bsonDoc.put("Balance",			co.getBalance());
		bsonDoc.put("Price",			co.getPrice());
		bsonDoc.put("OrderValue",		co.getOrderValue());
		bsonDoc.put("Currency", 		co.getCurrency());
		bsonDoc.put("Symbol",			co.getSymbol());
		bsonDoc.put("ISIN",				co.getIsin());
		bsonDoc.put("Rank",				co.getRank());
		bsonDoc.put("taxAmount",		co.getTaxAmount());
		bsonDoc.put("RealizedGL",		co.getRealizedGain());
		bsonDoc.put("UnrealizedGL",		co.getUnrealizedGain());
		bsonDoc.put("ShortTermRate",    co.getShortTermTaxRate());
		bsonDoc.put("LongTermRate",     co.getLongTermTaxRate());
		bsonDoc.put("TaxRateDesc",		co.getTaxRateString());
	}
	
	/**
	 * Query the closingOrder collection by accountId and the symbol
	 * @param accountId
	 * @param symbol
	 * @param closingOrderTicket
	 * @return A linked list of matches
	 */
	public LinkedList<ClosingOrder> findByAccountIdAndSymbol(
			String accountId,
			String symbol,
			ClosingOrderTicket closingOrderTicket) {
		LinkedList<ClosingOrder> retList = new LinkedList<ClosingOrder>();
		
		BasicDBObject whereClause = new BasicDBObject();
		whereClause.put("AccountId", accountId);
		whereClause.put("Symbol", symbol);
		
		BasicDBObject fields = new BasicDBObject();
		fields.put("employeeId", 1);

		/**
		 * Now query the data with ...find(whereClause)
		 */
		DBCursor cursor = this.getDBCollection().find(whereClause); //, fields);

		try {
			while (cursor.hasNext()) {
				DBObject doc = (DBObject)cursor.next();

				ClosingOrder co = new ClosingOrder();
				
				co.status = TaxLotStatus.valueOf((String) doc.get("Status"));
				co.setHostOrderId((String)		doc.get("HostOrderId"));
				co.setAccountId((String)		doc.get("AccountId"));
				co.setTradeId((String)			doc.get("TradeId"));
				co.setExchangeMIC((String)		doc.get("ExchangeMIC"));
				co.setMarketMIC((String)		doc.get("MarketMIC"));
				co.setAssetType((String) 		doc.get("AssetType"));
				co.setTradeText((String)		doc.get("ProductDesc"));
				co.setCorporateActionType((String)doc.get("CorpActionType"));
				co.setDate((java.util.Date)     doc.get("Date"));
				co.setQuantity((int)			doc.get("Quantity"));
				co.setBalance((int)				doc.get("Balance"));
				co.setPrice((double)			doc.get("Price"));
				co.setOrderValue((double)		doc.get("OrderValue"));
				co.setCurrency((String)			doc.get("Currency"));
				co.setSymbol((String)			doc.get("Symbol"));
				co.setIsin((String)				doc.get("ISIN"));
				co.setRank((int)				doc.get("Rank"));
				co.setTaxAmount((double)		doc.get("taxAmount"));
				co.setRealizedGain((double)		doc.get("RealizedGL"));
				co.setUnrealizedGain((double)	doc.get("UnrealizedGL"));
				co.setShortTermTaxRate((double) doc.get("ShortTermRate"));
				co.setLongTermTaxRate((double) 	doc.get("LongTermRate"));
				co.setTaxRateString((String)	doc.get("TaxRateDesc"));

				retList.add(co);
//			System.out.println(cursor.next());
			}
		} finally {
			cursor.close();
		}			
		return retList;
	}
	
	/**
	 * Getter and Setter methods
	 * @return
	 */
	public ClosingOrderTicket getCot() {
		return cot;
	}

	public void setCot(ClosingOrderTicket cot) {
		this.cot = cot;
	}


}
