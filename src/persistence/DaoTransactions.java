package persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import calcEngine.TaxRate;
import orders.OpeningOrder;
import orders.OpeningOrderTicket;
import orders.ClosingOrder;
import orders.ClosingOrderTicket;
import orders.Transaction;

/**
 * Data Access Object - Supports the CRUD operations with a binding to 
 * SQL Server - Purpose is to retrieve transactions and generate result sets
 * based on transaction types.  For Equity Open and Close orders, this is 
 * determined by the price, where Open is positive and Close is a negative #
 * @author Mark
 */

public class DaoTransactions extends Collection {
	String collName = "closingOrders";
	private SqlServerClient ssClient = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	/**
	 * Transaction data used for transfer to generated Open or Close Orders
	 */
	String tradeId = "";
	String hostTradeId = "";
	String hostOrderId = "";
	String accountId = "";
	String assetType = "";
	String exchangeMIC = "";
	String marketMIC = "";
	String corpActionType = "";
	String symbol = "";
	String isin = "";
	String currency = "";
	String tradeText = "";
	Date date = null;
	int quantity = 0;
	double price = 0.0;
	OpeningOrder openOrd = null;
	ClosingOrder closeOrd = null;

	public DaoTransactions() {
		// TODO Auto-generated constructor stub
	}
	
	public DaoTransactions(MongoDBUtils mongoUtils, ClosingOrder co) {
    	super(mongoUtils);
		this.setDBCollection(this.mongoDriverUtil.getDb().getCollection(this.collName));
		this.closeOrd = co;
	}
	

	/****** Script for SelectTopNRows command from SSMS
	 * 
	 *   Query using the ViewTrades view and pick out the elements we need.
	 *   Then for each transaction, determine by the price if it is a OPEN or
	 *   a CLOSE order, and add that transaction to the OpeningOrder Ticket or the
	 *   CLOSE Order ticket. 
	 * @param top 
	 * @param myTaxRate 
	 *   
	 * @throws SQLException *****
	 */

	public ResultSet sqlViewTrades(String accntId, int top, TaxRate myTaxRate, OpeningOrderTicket bot, ClosingOrderTicket sot)
			throws SQLException {
		boolean first = true;
		
		String topString = "TOP ";
		
		if(top != 0) 
			topString = topString.concat(new Integer(top).toString());
		else
			topString = "";
			
		// Create and execute an SQL statement that returns some data.
		String SQL = "SELECT " + topString + "[TradeId],[Host],[HostOrderId],[HostTradeId],[ExchangeOrderId]" +
				",[ExchangeTradeId],[AccountId],[ExchangeMIC],[MarketMIC],[Symbol],[Currency],[ISIN]" +
				",[TradeTime],[TradeText],[Quantity],[Price],[AssetType],[CorporateActionType]" +
				" FROM [LiquidView].[dbo].[ViewTradesStockAndOptionsAndFutures]" +
				" WHERE AccountId = '" + accntId + "'";

		this.stmt = getSsClient().getConn().createStatement();
		this.rs = this.stmt.executeQuery(SQL);

		// Iterate through the data in the result set and display it.
		while (this.rs.next()) {
//			System.out.println(this.rs.getString(1) + " " 
//					+ this.rs.getString(2)  + " " 
//					+ this.rs.getString(3)  + " " 
//					+ this.rs.getString(4)  + " " 
//					+ this.rs.getString(5)  + " "
//					+ this.rs.getString(6)  + " "
//					+ this.rs.getString(7)  + "\n\t "
//					+ this.rs.getString(8)  + " "
//					+ this.rs.getString(9)  + " "
//					+ this.rs.getString(10)  + " "
//					+ this.rs.getString(11)  + " "
//					+ this.rs.getString(12)  + " "
//					+ this.rs.getString(13)  + " "
//					+ this.rs.getString(14)  + " "
//					+ this.rs.getString(15)  + " "
//					+ this.rs.getString(16)  + " ["
//					+ this.rs.getString(17)  + "]"
//					);
			
			tradeId 	= this.rs.getString("TradeId");
			hostOrderId = this.rs.getString("HostOrderId"); 
			hostTradeId	= this.rs.getString("HostTradeId"); 
			exchangeMIC = this.rs.getString("ExchangeMIC");
			marketMIC   = this.rs.getString("MarketMIC");
			accountId	= this.rs.getString("AccountId"); 
			symbol		= this.rs.getString("Symbol"); 
			isin		= this.rs.getString("ISIN"); 
			currency	= this.rs.getString("Currency");
			date		= this.rs.getDate  ("TradeTime");
			quantity	= this.rs.getInt   ("Quantity");
			price 		= this.rs.getDouble("Price");
			assetType   = this.rs.getString("AssetType");
			tradeText   = this.rs.getString("TradeText");
			corpActionType = this.rs.getString("CorporateActionType");
			

			/**
			 * Add the transaction to the OPEN or CLOSE list based on quantity (+ or -)
			 */
			if( quantity < 0.0) {
				sot.setHostOrderId(hostOrderId);
				
				closeOrd = new ClosingOrder(Transaction.CLOSE, hostOrderId, tradeId, 
						corpActionType, quantity, symbol, date, price);
				closeOrd.setAccountId(accountId);
				closeOrd.setBalance(0);
				closeOrd.setPrice(price);
				closeOrd.setCurrency(currency);
				closeOrd.setIsin(isin);
				closeOrd.setShortTermTaxRate(myTaxRate.getShortTermRate());
				closeOrd.setLongTermTaxRate( myTaxRate.getLongTermRate());
				closeOrd.setAssetType(assetType);
				closeOrd.setHostOrderId(hostOrderId);
				closeOrd.setMarketMIC(marketMIC);
				closeOrd.setExchangeMIC(exchangeMIC);
				closeOrd.setTradeText(tradeText);
				sot.closingOrderListAddClose(closeOrd);
			}
			else {
				openOrd = new OpeningOrder(Transaction.OPEN, hostOrderId, tradeId, 
								corpActionType, quantity, symbol, date, price);
				openOrd.setAccountId(accountId);
				openOrd.setBalance(quantity);
				openOrd.setPrice(price);
				openOrd.setCurrency(currency);
				openOrd.setIsin(isin);
				openOrd.setShortTermTaxRate(myTaxRate.getShortTermRate());
				openOrd.setLongTermTaxRate( myTaxRate.getLongTermRate());
				openOrd.setAssetType(assetType);
				openOrd.setHostOrderId(hostOrderId);
				openOrd.setMarketMIC(marketMIC);
				openOrd.setExchangeMIC(exchangeMIC);
				openOrd.setTradeText(tradeText);
				
				bot.openOrdListAddOpen(openOrd);
				
				/**
				 * Possible design flaw here.  In attempting to follow the Tax Lots
				 * Document the concept of an Opening Transaction gets captured, but
				 * is likely an ill conceived concept.  Rather than copying the first
				 * object, we should store a reference to the first item in the linked list
				 * 
				 * This needs to be reviewed, as I can see no use for separately storing the first item.
				 * The OpeningOrderTicket needs to be updated only 
				 */
				if(first) {
					bot.setHostOrderId(hostOrderId);
					bot.setOpeningOpenOrd(openOrd);
					first = false;
				}				
			}
		}
		return rs;
	};

	/**
	 * 
	 */
	public void insertTransaction(ClosingOrder co ) {
		
	}
	
	
	public SqlServerClient getSsClient() {
		return ssClient;
	}

	public void setSsClient(SqlServerClient ssClient) {
		this.ssClient = ssClient;
	}
}
