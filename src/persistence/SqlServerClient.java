package persistence;

import java.sql.*;

/**
 * @author Mark Bosakowski
 * SqlServer Client Utility  - Needs VPN access to test server at: ssl34.liqdplatform.com
 */
public class SqlServerClient {
	/**
	 * 	Define default SqlServer host and database name configuration parameters 
	 */
	private java.sql.Connection ssConn = null;
	private SqlServerClient ssClient = null;
	private int portNum = 0;   
	private String hostName = "";
	private String myUserName = "";
	private String myPassword = "";
	private String dbName = "";
	private String connectionUrl = null;
	private int debugLevel = 0;

	/**
	 * JDBC Client for SqlServer database - Initialized default parameters
	 */
	public SqlServerClient() {
	}


	/**
	 * ssConnect() - Connect using the "get" methods which allows the caller
	 *  			 to override the default connection parameters
	 */
	public boolean ssConnect() {
		boolean rc = false;
		/** 
		 * Load the JDBC driver and establish a connection. 
		 */
		try 
		{   
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); 

			String url = "jdbc:sqlserver://"  + 
					this.getHostName() 		+ ":" + 
					this.getPortNum()  		+ ";databaseName=" +  
					this.getDbName()   		+ ";user=" +
					this.getMyUserName()	+ ";password=" +
					this.getMyPassword();
				
			this.setConnectionUrl(url);
			this.setConn(DriverManager.getConnection(this.getConnectionUrl()));
			
			this.setSsClient(this);

//			String url ="jdbc:sqlserver://10.3.1.135:1433;" +
//					"databaseName=LiquidView;user=lvreader;password=lvreader33"
				
//			this.setConn(DriverManager.getConnection(url, this.getMyUserName(), this.getMyPassword() ));
						
			rc = true;
		} 
		catch( Exception e ) 
		{ 
			e.printStackTrace();
		}  
		return rc; 
	}

	/**
	 * Encapsulate an SQL call
	 * @param SqlServerSQL
	 */
	public void executeSQL( String ssSQL)  {
		/** 
		 * Create a statement and execute a select query. 
		 */ 
		Statement st;
		try {
			st = ssClient.ssConn.createStatement();

			ResultSet rs = st.executeQuery( ssSQL); 
			while( rs.next() ) 
			{  
				System.out.println(rs.getString(1));
			}
//			if (rs != null) {
//				rs.close();
//			}
//			if (st != null) {
//				st.close();
//			}
//			if (ssClient.getConn() != null) {
//				ssClient.getConn().close();
//			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public void getSqlServerVersion() {
		
		ssClient.executeSQL("SELECT @@VERSION");
	}


	/************************* Getter and Setter Methods *********************/
	/**
	 * @return the ssClient
	 */
	public SqlServerClient getPgClient() {
		return ssClient;
	}

	/**
	 * @return the connection
	 */
	public java.sql.Connection getConn() {
		return ssConn;
	}

	/**
	 * @param ssConn the ssConn to set
	 */
	public void setConn(java.sql.Connection conn) {
		this.ssConn = conn;
	}
	/**
	 * @return the portNum
	 */
	public int getPortNum() {
		return portNum;
	}
	/**
	 * @param portNum the portNum to set
	 */
	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}
	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}
	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	/**
	 * @return the myUserName
	 */
	public String getMyUserName() {
		return myUserName;
	}
	/**
	 * @param myUserName the myUserName to set
	 */
	public void setMyUserName(String myUserName) {
		this.myUserName = myUserName;
	}
	/**
	 * @return the myPassword
	 */
	public String getMyPassword() {
		return myPassword;
	}
	/**
	 * @param myPassword the myPassword to set
	 */
	public void setMyPassword(String myPassword) {
		this.myPassword = myPassword;
	}

	/**
	 * @return the SqlServer Client instance
	 */
	public SqlServerClient getSsClient() {
		return ssClient;
	}
	/**
	 *  set the SsClient instance with an initialized value
	 */
	public void setSsClient(SqlServerClient ssClient) {
		this.ssClient = ssClient;
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	
	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}
	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	/**
	 * @param debugLevel the debugLevel to set
	 */
	public void setDebugLevel(int debugLevel) {
		this.debugLevel = debugLevel;
	}

	/**
	 * @return the debugLevel
	 */
	public int getDebugLevel() {
		return debugLevel;
	}


	/**
	 * main(args) - Used to unit test connection 
	 * @param args
	 */
	public static void main(String[] args) {
		SqlServerClient ssClient = new SqlServerClient();

		/**
		 * Test the over-ride of the SqlServer-Driver connection parameters 
		 */
		ssClient.setPortNum(1433); 
		ssClient.setHostName("10.3.1.135");
		ssClient.setDbName("LiquidView");
		ssClient.setMyUserName("lvreader");
		ssClient.setMyPassword("lvreader33");
		ssClient.ssConnect();

		ssClient.setSsClient(ssClient);
//		ssClient.executeSQL("SELECT table_name FROM information_schema.tables " +
//				"WHERE table_schema='public' " +
//				" AND " + 
//				" table_type='BASE TABLE';" ); 

		
		// Create and execute an SQL statement that returns some data.
		Statement stmt = null;
		ResultSet rs = null;
		String SQL = "SELECT LastName, FirstName, AccountId, UserId, Source FROM dbo.ViewAccounts";
		try {
			stmt = ssClient.getConn().createStatement();
	
			rs = stmt.executeQuery(SQL);
			System.out.println("SQL=" + SQL);
			// Iterate through the data in the result set and display it.
			while (rs.next()) {
				System.out.println(rs.getString(1) + " " 
						+ rs.getString(2)  + " " 
						+ rs.getString(3)  + " " 
						+ rs.getString(4)  + " " 
						+ rs.getString(5)  + " " 
						);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		ssClient.getSqlServerVersion();
	}
}

