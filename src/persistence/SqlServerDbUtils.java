package persistence;

import java.sql.*;

public class SqlServerDbUtils {
	/**
	 * Create a variable for the connection string.
	 */
	private String connectionUrl = null;
	Connection con = null;  // Declare the JDBC objects.
	Statement stmt = null;
	ResultSet rs = null;
	public SqlServerDbUtils() {
		// TODO Auto-generated constructor stub
	}


	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	public static void main(String[] args) {
		SqlServerDbUtils ssDbu = new SqlServerDbUtils();

		ssDbu.setConnectionUrl("jdbc:sqlserver://10.3.1.135:1433;" +
				"databaseName=LiquidView;user=lvreader;password=lvreader33");

		try {
			// Establish the connection.
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			ssDbu.con = DriverManager.getConnection(ssDbu.getConnectionUrl());

			// Create and execute an SQL statement that returns some data.
			String SQL = "SELECT LastName, FirstName, AccountId, UserId, Source FROM dbo.ViewAccounts";
			ssDbu.stmt = ssDbu.con.createStatement();
			ssDbu.rs = ssDbu.stmt.executeQuery(SQL);

			// Iterate through the data in the result set and display it.
			while (ssDbu.rs.next()) {
				System.out.println(ssDbu.rs.getString(1) + " " 
						+ ssDbu.rs.getString(2)  + " " 
						+ ssDbu.rs.getString(3)  + " " 
						+ ssDbu.rs.getString(4)  + " " 
						+ ssDbu.rs.getString(5)  + " " 
						);
			}
		}

		// Handle any errors that may have occurred.
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (ssDbu.rs   != null) try { ssDbu.rs.close();  } catch(Exception e) {}
			if (ssDbu.stmt != null)	try { ssDbu.stmt.close();} catch(Exception e) {}
			if (ssDbu.con  != null) try { ssDbu.con.close(); } catch(Exception e) {}
		}
	}
}
