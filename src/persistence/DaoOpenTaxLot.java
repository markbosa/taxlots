package persistence;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import calcEngine.TaxLot;

public class DaoOpenTaxLot extends Collection {
	String collName = "openTaxLots";

	public DaoOpenTaxLot(MongoDBUtils mongoUtils) {
    	super(mongoUtils);
		this.setDBCollection(this.mongoDriverUtil.getDb().getCollection(this.collName));
	}

	/**
	 * Insert method for TaxLot individual items
	 * @param hostOrderId 
	 * @param tl 
	 * @return
	 */
	public boolean insertTaxLot(String hostOrderId, TaxLot tl) {

		try {
			/**
			 * Create a BSON formatted document with image and text data
			 */
			DBObject bsonDoc = new BasicDBObject();

			//			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"); 
			//			Calendar cal = Calendar.getInstance();  
			//			System.out.println(dateFormat.format(cal.getTime()));
			bsonDoc.put("Status",           tl.getStatus().toString());
			bsonDoc.put("HostOrderId",		hostOrderId);
			bsonDoc.put("AccountId", 		tl.getAccount() );
			bsonDoc.put("ExchangeId",		tl.getOpenOrder().getExchangeMIC());
			bsonDoc.put("TradeId",			tl.getOpenOrder().getTradeId());		
			bsonDoc.put("LotID", 			tl.getLotId());
			bsonDoc.put("LotDate",			tl.getLotDate() );		
			bsonDoc.put("Currency", 		tl.getOpenOrder().getCurrency());
			bsonDoc.put("MarketId",			tl.getOpenOrder().getMarketMIC());
			bsonDoc.put("Symbol",			tl.getOpenOrder().getSymbol());
			bsonDoc.put("ISIN",				tl.getOpenOrder().getIsin());
			bsonDoc.put("AssetType", 		tl.getOpenOrder().getAssetType());
			bsonDoc.put("LotOriginalQty", 	tl.getLotOriginalQty());
			bsonDoc.put("LotOriginalCost", 	tl.getLotOriginalCost());
			bsonDoc.put("LotUnitCost",		tl.getLotUnitCost());
			bsonDoc.put("PrevDayPosition", 	tl.getPreviousDayPosition());
			bsonDoc.put("Bought", 			tl.getBoughtQty());
			bsonDoc.put("TodayPosition",	tl.getOpenOrder().getBalance());
			bsonDoc.put("Price",			tl.getPrice());
			bsonDoc.put("MarketValue", 		tl.getMarketValue());
			bsonDoc.put("RealizedGL",		tl.getOpenOrder().getRealizedGain());
			bsonDoc.put("UnrealizedGL",		tl.getOpenOrder().getUnrealizedGain());
			bsonDoc.put("ProductDesc", 		tl.getOpenOrder().getTradeText());
			if(tl.getOpenOrder().getCorporateActionType() != null)
				bsonDoc.put("CorpActionType",	tl.getOpenOrder().getCorporateActionType() );

 
			System.out.println("DaoOpenTaxLot::insertTaxLot: " + tl.getLotId() + " : " + 
								tl.getOpenOrder().getSymbol() + " : " +
								tl.getOpenOrder().getBalance());

			this.getDBCollection().insert(bsonDoc);

			/**
			 * Now set the Mongo "_id" field of the newly inserted object
			 */
			//tl.setLotId((ObjectId) bsonDoc.get("_id"));
			return true;
		}
		finally {

		}
	}
}
