/**
 * 
 */
package persistence;

import org.bson.types.ObjectId;

import orders.Transaction;
import calcEngine.TaxLot;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

/**
 * DaoClosedTaxLot - A Data Access Object to transfer a TaxLot object
 * to a persistent state in MongoDB via an insert statement.
 * 
 * The basic algorithm is that new CLOSE orders are matched against historical
 * Open OPEN orders, and when we have a match that is fully resolved, then the
 * state of the Open TaxLot record is updated and persisted as a Closed TaxLot
 * 
 * After confirmation that the Tax Lot has been persisted, we remove the Open
 * Tax Lot object from the OpenTaxLots collection.
 * 
 * @author Mark Bosakowski
 */
public class DaoClosedTaxLot extends Collection {
	String collName = "closedTaxLots";

	/**
	 * Constructor requires a working MongoDB client connection
	 * @param mongoDbu
	 */
	public DaoClosedTaxLot(MongoDBUtils mongoDbu) {
		super(mongoDbu);
		this.setDBCollection(this.mongoDriverUtil.getDb().getCollection(this.collName));
	}


	/**
	 * Insert method for ClosedTaxLot individual items
	 * @param ctl - A single TaxLot item
	 * @return boolean - False if fails on insert
	 */
	public boolean insertClosedTaxLot(TaxLot ctl) {
		/**
		 * Create a BSON formatted document with closed tax lot data
		 */
		DBObject bsonDoc = new BasicDBObject();		
		this.transferTaxLot(bsonDoc, ctl);

		/** 
		 * Create a list of tax methods along with the calculated values
		 * for derived from each tax method	  
		 */		
		DBObject taxDoc = this.generateTaxMethod(ctl);
		
		/**
		 * Add the tax lot method name used to tag the methods calculated taxes
		 */
		BasicDBList  taxMethodList= new BasicDBList();
		taxMethodList.add(taxDoc);		
		bsonDoc.put(Transaction.getTaxLotMethodId().toString(), taxMethodList);
		
		try {
			System.out.println("DaoClosedTaxLot::insertTaxLot: " + ctl.getLotId() + " : " + 
					ctl.getOpenOrder().getSymbol() + " : " + " Open Balance=" +
					ctl.getOpenOrder().getBalance());

			WriteResult result = this.getDBCollection().insert(bsonDoc);

			if(result.getN() == 0) {
				/**
				 * Now set the Mongo "_id" field of the newly inserted object
				 */
				ctl.setId((ObjectId) bsonDoc.get("_id"));			
				return true;
			}
			else {
				System.out.println(
						"DaoClosedTaxLot:: Error updating TradeId[" + ctl.getOpenOrder().getTradeId() +
						"] Error= " + result.toString() + 
						" Suggest stop processing - check SequenceManger collection");
				return false;
			}
		}
		catch( Exception e) {
			return false;
		}
	}

	/**
	 * We already created the summary data in the insert command so only
	 * update (or add) new tax lot pairings at the end of the document
	 * This provides a history of the tax lot pairings used to close out
	 * the lot.
	 * 
	 * @param hostOrderId
	 * @param ctl - Current Tax Lot
	 */
	public boolean updateClosedTaxLot(TaxLot ctl) {
		/**
		 * Create a BSON formatted document with image and text data
		 */
		DBObject searchObject = new BasicDBObject();

		/**
		 * Update assumes document exists, so leave the common data alone and
		 * only update via $addToSet the next tax lot pairing for each method.  
		 * This allows for an array of calculated results in the order processed 
		 */
		
		DBObject taxDoc = this.generateTaxMethod(ctl);

		/**
		 * Status will change on last lot pairing, so update each time
		 */
		searchObject.put("Status", ctl.getStatus().toString());
		searchObject.put(Transaction.getTaxLotMethodId().toString(), taxDoc);
		
		/**
		 * Find the record to update with this query
		 */
		BasicDBObject query = new BasicDBObject();
		query.put("HostOrderId", ctl.getOpenOrder().getHostOrderId());
		query.put("TradeId", ctl.getOpenOrder().getTradeId());	

		System.out.println("DaoClosedTaxLot::updateTaxLot: " + ctl.getLotId() + " : " + 
				ctl.getOpenOrder().getSymbol() + " : " + " Open Balance=" +
				ctl.getOpenOrder().getBalance());

		/**
		 * Third parameter is set to "true" which makes this an upsert vs.
		 * a traditional update operation.  Upsert is desired here as the
		 * 1st run may be LIFO, 2nd, FIFO, etc. For complete tax lot analysis
		 * we may add up to 6 standard tax lot methods.
		 * 
		 * Because the derived information from the insert() does not change,
		 * this method allows us to upsert as needed without having to modify
		 * the common data for all tax lot methods.
		 */
		WriteResult result = this.getDBCollection().update(query, 
				new BasicDBObject("$addToSet", searchObject), true, false);

		/**
		 * Two tests to prove that the update was only on one document that existed
		 */
		if((result.getN() == 1) && (result.isUpdateOfExisting() == true)) {
			/**
			 * Now set the Mongo "_id" field of the newly inserted object
			 */
			ctl.setId((ObjectId) result.getUpsertedId());
			return true;
		}
		else {
			System.out.println(
					"DaoClosedTaxLot:: Error updating TradeId[" + ctl.getOpenOrder().getTradeId() +
					"] Error= " + result.toString() + 
					" Suggest stop processing - check SequenceManger collection");
			return false;
		}
	}

	/**
	 * transferTaxLot() - A utility method to ensure that inserts and 
	 * updates data in the same data format
	 * 
	 * @param bsonDoc - Document to be persisted
	 * @param ctl - A Closed Tax Lot
	 */
	private void transferTaxLot(DBObject bsonDoc, TaxLot ctl) {

		bsonDoc.put("Status",           ctl.getStatus().toString());
		bsonDoc.put("HostOrderId",		ctl.getOpenOrder().getHostOrderId());
		bsonDoc.put("AccountId", 		ctl.getAccount() );
		bsonDoc.put("ExchangeId",		ctl.getOpenOrder().getExchangeMIC());
		bsonDoc.put("TradeId",			ctl.getOpenOrder().getTradeId());
		bsonDoc.put("Currency", 		ctl.getOpenOrder().getCurrency());
		bsonDoc.put("MarketId",			ctl.getOpenOrder().getMarketMIC());
		bsonDoc.put("Symbol",			ctl.getOpenOrder().getSymbol());
		bsonDoc.put("ISIN",				ctl.getOpenOrder().getIsin());
		bsonDoc.put("AssetType", 		ctl.getOpenOrder().getAssetType());

		bsonDoc.put("LotID", 			ctl.getLotId());
		bsonDoc.put("LotDate",			ctl.getLotDate() );
		bsonDoc.put("LotOriginalQty", 	ctl.getLotOriginalQty());
		bsonDoc.put("LotOriginalCost", 	ctl.getLotOriginalCost());
		bsonDoc.put("LotUnitCost",		ctl.getLotUnitCost());

		bsonDoc.put("PrevDayPosition", 	ctl.getPreviousDayPosition());
		bsonDoc.put("Bought", 			ctl.getBoughtQty());
		bsonDoc.put("Price",			ctl.getPrice());
		bsonDoc.put("TodayPosition",	ctl.getOpenOrder().getBalance());
		bsonDoc.put("ProductDesc", 		ctl.getOpenOrder().getTradeText());
		
		if(ctl.getOpenOrder().getCorporateActionType() != null)
			bsonDoc.put("CorpActionType",	ctl.getOpenOrder().getCorporateActionType() );
	}

	/**
	 * generateTaxMethod - helper method to build the nested tax method information	
	 * @param bsonDoc - The document to add the sub-document to
	 * @param ctl - A Closed Tax Lot
	 */	
	private DBObject generateTaxMethod(TaxLot ctl){

		DBObject methodTaxes = new BasicDBObject();
		
		methodTaxes.put("Method", 			Transaction.getTaxLotMethodId().toString());
		methodTaxes.put("TradeId",          ctl.getCurrentClosingOrder().getTradeId());
		methodTaxes.put("Price",			ctl.getCurrentClosingOrder().getPrice());
		methodTaxes.put("Sold",				ctl.getCurrentClosingOrder().getBalance());
		methodTaxes.put("LotTaxAmount",		ctl.getTaxLotAmount());
		methodTaxes.put("LotRealizedGL",	ctl.getRealizedGain());
		methodTaxes.put("LotUnrealizedGL",	ctl.getUnrealizedGain());	
		methodTaxes.put("MarketValue", 		ctl.getMarketValue());

		return methodTaxes;
	}
}


