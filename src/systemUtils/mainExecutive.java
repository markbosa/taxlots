package systemUtils;

import java.util.Date;

import org.apache.log4j.PropertyConfigurator;

import orders.OpeningOrderTicket;
import orders.ClosingOrderTicket;
import orders.Transaction;
import persistence.DaoClosingOrder;
import persistence.DaoOpeningOrder;
import persistence.DaoTransactions;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;
import calcEngine.CalcMethodFIFO;
import calcEngine.CalcMethodHighCost;
import calcEngine.CalcMethodLIFO;
import calcEngine.CalcMethodLongTermHighCost;
import calcEngine.EnumTaxLotMethod;
import calcEngine.EnumTaxRate;
import calcEngine.TaxLotBucket;
import calcEngine.TaxRate;
import calcEngine.TaxRate.taxRateTier;

public class mainExecutive {
	long orderCnt = 1;
	int transactionCnt = 1;
	TaxLotBucket taxLotBucket = new TaxLotBucket();
	TaxRate myTaxRate = new TaxRate();
	taxRateTier currentTaxRate = myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
	private SqlServerClient ssClient = null;
	private MongoDBUtils mDbu = null;
	private int debugLevel = 0;
	private String accountId = "";
	private String symbol = "";
	private EnumTaxLotMethod taxLotMethod = EnumTaxLotMethod.NO_TAX_LOT_METHOD;
	protected Log4JLogger log = new Log4JLogger();

	public enum TestScenario {
		TAX_LOTS_DOC,
		TAX_LOTS_INTRADAY,
		TAX_LOTS_XLS,
		TAX_LOTS,RANDOM
	}

	public mainExecutive() {
		/**
		 * Print out system information
		 */
		SystemInfo SysInf = new SystemInfo();
		SysInf.PrintSystemInfo();
		
		/**
		 * Start the logging facility
		 */
		if( System.getProperty("os.name").startsWith("Windows") == true) {
			PropertyConfigurator.configure("C:\\Dev\\workspace\\TaxLot\\bin\\log4j.properties");
		}
		else {
			PropertyConfigurator.configure("/opt/TaxLot/bin/log4j.properties");
		}
	}

	public int getDebugLevel() {
		return debugLevel;
	}

	public void setDebugLevel(int debugLevel) {
		this.debugLevel = debugLevel;
	}
	/**
	 * Calculate and Format the elapsed time duration
	 * @param i - A date/time long integer
	 * @return  - A formatted human readable time string
	 */
	public static String durationHMSm( long i )  
	{  
		int     day = (int)( i / (1000L * 60 * 60 * 24) );  
		i %= 1000L * 60 * 60 * 24;  
		int     hrs = (int)( i / (1000L * 60 * 60) );  
		i %= 1000L * 60 * 60;  
		int     min = (int)( i / (1000L * 60) );  
		i %= 1000L * 60;  
		int     sec = (int)( i / 1000L );  
		i %= 1000L;  
		int     mil = (int)( i );  

		if( day > 0 )  
			return String.format( "%dd %02d:%02d:%02d.%03d", day, hrs, min, sec, mil );  
		return String.format( "%02d:%02d:%02d.%03d", hrs, min, sec, mil );  
	}  

	/**
	 * Method to manage the connection to the SQL Server Liquid View database
	 * @return true if we have a good connection, false if not.
	 */
	public boolean connectToLiquidView() {
		this.ssClient = new SqlServerClient();

		/**
		 * Over-ride of the SqlServer-Driver connection parameters 
		 */
		ssClient.setPortNum(1433); 
		ssClient.setHostName("10.3.1.135");
		ssClient.setDbName("LiquidView");
		ssClient.setMyUserName("lvreader");
		ssClient.setMyPassword("lvreader33");

		boolean rc =  this.ssClient.ssConnect();
		
		return rc; 
	}
	
	/**
	 * Method to handle the details of connecting to the TaxLots database
	 * in MongoDB
	 */
	public boolean connectToMongoDB() {
		this.mDbu = new MongoDBUtils();
		boolean home = true;

		/**
		 * Set (or over-ride) the Mongo-Driver connection parameters 
		 */
		if(home == true) {
			this.mDbu.setPortNum(27017);
			this.mDbu.setHostName("localhost");			
		}
		else {
			this.mDbu.setPortNum(27017);
			this.mDbu.setHostName("10.27.2.221");
//			this.mDbu.setHostName("Mongo-Conf-27-1");  does not work... DNS resolves to NJ			
		}
		this.mDbu.setDbName("taxLots");
	
		/**
		 * Now instantiate a single MongoDB client connection
		 * This has the side-effect of setting the two key instance variables
		 * "mongoClient" and the "this.connection" used by Search Type Queries 
		 */
		Boolean mrc = this.mDbu.createMongoClient();
		System.out.println("main()--> " +
				((mrc == true) ? " Successfully Connected" : "Failed to Connect ") +	
				" to MongoDB Database:: " + this.mDbu.getDbName() + 
				" on Host:: " + this.mDbu.getHostName() + 
				" on Port:: " + this.mDbu.getPortNum());
		return mrc;
	}
	
	  /**
	   * parseCommandLineArgs() - private method to override compiled defaults
	   * 
	   * Provide a command line interface to support Actuate BIRT reporting
	   * via an API with these parameters.  Upper, lower and mixed case will
	   * all be converted to lower case
	   * 
	   * --account 			Maps to AccountID
	   * --symbol			A symbol used to query tax lots
	   * --debugLevel 		A value from 0->4; higher levels gives more output
	   * --simulated		A flag to run only simulated data
	   * -- taxLotMethod	FIFO, LIFO, HighCost, BestTax, LONG_TERM_HIGH_COST
	   * 					Fifo_Intraday
	   * 
	   * @param args		an array of strings in the format "--argumentName argumentValue"
	   */
	  private void parseCommandLineArgs(String[] args) {
		  boolean flipFlop = true;
		  String command = null;

		  log.Info("================== Command Line Arguments ======================================");

		  for (String s: args) {
			  if(flipFlop == true) {
				  command = s.toLowerCase();
				  flipFlop = false;
			  }
			  else {
				  log.Info("Command / Argument: " + command + " " + s);
				  
				  if(command.startsWith("--accountid") == true) {
					  this.accountId  = s;
				  }
				  else if(command.startsWith("--symbol") == true) {
					  this.symbol = s;
				  }
				  else if(command.startsWith("--debuglevel") == true) {
					  this.setDebugLevel(Integer.parseInt(s));
				  }
				  else if(command.startsWith("--taxlotmethod") == true) {
					  this.taxLotMethod = EnumTaxLotMethod.setMethodByString(s);
				  }
				  else if(command.startsWith("--simulated") == true) {
				  }
				  else {
					  log.Debug("Invalid Command:: " + s);
					  System.exit(-1);
				  }
				  flipFlop = true;
			  }
		  }
		  log.Info("================== Command Line Arguments Processed ============================\n");
	  }
	  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * Create an instance of the mainExecutive class:
		 * 1) Verify the taxRateTiers
		 * 2) Parse command line arguments (system overrides)
		 * 3) Connect to SQL Server and MongoDB database
		 * 4) Execute the selected tax lot method
		 */
		mainExecutive exec = new mainExecutive();

		/** 
		 * Set compiled program defaults for tax lot method and tax rates
		 * Good Test Cases...
		 * Account "LTI673";   symbol = "HAWK.LN"; 
		 * Account "LHG1036";  symbol = "SPY.UP";
		 * 
		 */
		exec.accountId = "LTI555";
		exec.symbol = "GOOG.UQ"; // 2 open & 3 close creates 2 closed tax lots		 */
		exec.taxLotMethod = EnumTaxLotMethod.OPENING_ORDER_ETL_ONLY;   // FIFO;

		/**
		 * Need to query LiquidView to get the daily trades by symbol & account  
		 * To limit query results, add the "top" limit to the query
		 */
		int top = 0;
		
		/**
		 * The setCapitalGainRates method sets both the short & long term rates
		 * based upon the income rate of the equity owner. Default= highest rate
		 */
		exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
		exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());
		exec.myTaxRate.printTaxRateTable();

		/**
		 * First order of business is to set any command line arguments
		 */
		exec.parseCommandLineArgs(args);
		
		/**
		 * First order of business is to see if we can connect to the Liquid
		 * View database.  If not, it is a show-stopper
		 */
		exec.log.Info("================== Connecting to SQL Server Liquid View Database ===============\n");

		if(exec.connectToLiquidView() == false) {
			System.out.println("Fatal Error: Could not connect to Liquid View database");
			System.exit(-1);
		}
		else			
			exec.ssClient.getSqlServerVersion();

		/**
		 * Second order of business is to see if we can connect to the MongoDB
		 * TaxLots database.
		 */
		exec.log.Info("================== Connecting to MongoDB taxLots Database ======================\n");

		if(exec.connectToMongoDB() == false) {
			System.out.println("Fatal Error: Could not connect to MongoDB TaxLot database");
			System.exit(-2);
		}
		else
			System.out.println("MongoDB Version: " + exec.mDbu.getDb().getMongo().getVersion() + "\n"); 
			
		exec.log.Info("================ Main Process Starting =========================================\n");

		/**
		 * Start timer here to factor out the expensive time to connect to 
		 * the 2 databases.  This allows a fair calculation of import ETL
		 * and tax lot closing processes.
		 */
		Date startTime = new Date();
		long start = startTime.getTime();
			
		DaoTransactions daoViewTrades = new DaoTransactions();
		
		/**
		 * Serially reused variables for testing only
		 * Expect distinct instance variable to derive from web services API
		 */
		OpeningOrderTicket openingOrderTicket = null;
		ClosingOrderTicket closingOrderTicket = null;
		DaoOpeningOrder daoOpeningOrder = null;
		DaoClosingOrder daoClosingOrder = null;
		
		/**
		 * For each "Test Scenario" we want to populate the open and close orders
		 * with different open and close tickets.  The TAX_LOTS_DOC references 
		 * the documented test cases in the "LiquidTrade Tax Lots Support"
		 * document where the same data is used to illustrate all 6 tax methods
		 * 
		 * The TAX_LOTS_XLS pre-populated data to match the test scenarios
		 * found in the "TaxLotScenarios.xls" spreadsheet.
		 * 
		 * The TAX_LOTS_INTRADAY modifies the tests to run the INTRADAY sort
		 * option followed by FIFO. 

		 */
		try {			  
 			switch(exec.taxLotMethod) {
 			case OPENING_ORDER_ETL_ONLY :
				/**********************************************************************
				 * Process the initial loading of historical data via ETL process
				 *********************************************************************/	
 				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.OPENING_ORDER_ETL_ONLY);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "ETL_O");
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "ETL_C");		
				
				daoViewTrades.setSsClient(exec.ssClient);
				daoViewTrades.sqlViewTrades(
						exec.accountId, top, 
						exec.myTaxRate, 
						openingOrderTicket, 
						closingOrderTicket);
				
				/**
				 * Because Closes can be done against historical data, we need
				 * to consider all open orders, closeOrd load todays opens into a 
				 * persistent data store where the closes can query against them
				 */
				openingOrderTicket.openOrderEtl   (exec.mDbu, openingOrderTicket);
				closingOrderTicket.closingOrderEtl(exec.mDbu, closingOrderTicket);
 				break;
			case FIFO: 
				/**********************************************************************
				 * Process the FIFO Tax Lot Method
				 *********************************************************************/	
				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.FIFO);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "O1");
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "O2");		
				
				/**
				 * Query MongoDB openOrders collection to build openingOrders[]  
				 */
				daoOpeningOrder = new DaoOpeningOrder(openingOrderTicket, exec.mDbu);
				
				openingOrderTicket.setOpenOrdList(
						daoOpeningOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol, openingOrderTicket));

				/**
				 * Query MongoDB closingOrders collection to build closingOrders[]  
				 */
				daoClosingOrder = new DaoClosingOrder(closingOrderTicket, exec.mDbu);
				
				closingOrderTicket.setClosingOrderList(
						daoClosingOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol,closingOrderTicket));				
				
				/**
				 * With list of opening and closing orders, apply the FIFO method
				 */
				CalcMethodFIFO fifoCalc = new CalcMethodFIFO(exec.ssClient, exec.mDbu);

				fifoCalc.setDumpItems(true);
				fifoCalc.setOpen( openingOrderTicket.getOpenOrdList());
				fifoCalc.setClose(closingOrderTicket.getClosingOrderList());

				fifoCalc.sortOrders();
				fifoCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
				break;
			case FIFO_INTRADAY: 
				/**********************************************************************
				 * Process the FIFO Intraday Tax Lot Method
				 *********************************************************************/	
				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.FIFO_INTRADAY);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "O3");						
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "O4");
				// mainExec.runTaxScenario(TestScenario.TAX_LOTS_INTRADAY, openOrderTicket, closeOrderTicket);
									
				/**
				 * Query MongoDB openOrders collection to build openingOrders[]  
				 */
				daoOpeningOrder = new DaoOpeningOrder(openingOrderTicket, exec.mDbu);
				
				openingOrderTicket.setOpenOrdList(
						daoOpeningOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol, openingOrderTicket));

				/**
				 * Query MongoDB closingOrders collection to build closingOrders[]  
				 */
				daoClosingOrder = new DaoClosingOrder(closingOrderTicket, exec.mDbu);
				
				closingOrderTicket.setClosingOrderList(
						daoClosingOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol,closingOrderTicket));						
				
				/**		
				 * set the Intraday option to post-modify the FIFO Sort
				 */
				CalcMethodFIFO fifoCalcIntraday = new CalcMethodFIFO(exec.ssClient, exec.mDbu);

				fifoCalcIntraday.setIntraday(true);
				fifoCalcIntraday.setDumpItems(true);

				fifoCalcIntraday.setOpen( openingOrderTicket.getOpenOrdList());
				fifoCalcIntraday.setClose(closingOrderTicket.getClosingOrderList());

				fifoCalcIntraday.sortOrders();
				fifoCalcIntraday.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
				break;
			case LIFO: 
				/**********************************************************************
				 * Process the LIFO Tax Lot Method 
				 *********************************************************************/
				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.LIFO);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "O5");						
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "O6");
				// mainExec.runTaxScenario(TestScenario.TAX_LOTS_DOC, openOrderTicket, closeOrderTicket);

				/**
				 * Query MongoDB openOrders collection to build openingOrders[]  
				 */
				daoOpeningOrder = new DaoOpeningOrder(openingOrderTicket, exec.mDbu);
				
				openingOrderTicket.setOpenOrdList(
						daoOpeningOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol, openingOrderTicket));

				/**
				 * Query MongoDB closingOrders collection to build closingOrders[]  
				 */
				daoClosingOrder = new DaoClosingOrder(closingOrderTicket, exec.mDbu);
				
				closingOrderTicket.setClosingOrderList(
						daoClosingOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol,closingOrderTicket));				
				
				CalcMethodLIFO lifoCalc = new CalcMethodLIFO(exec.ssClient, exec.mDbu);	

				lifoCalc.setDumpItems(true);
				lifoCalc.setOpen( openingOrderTicket.getOpenOrdList());
				lifoCalc.setClose(closingOrderTicket.getClosingOrderList());

				lifoCalc.sortOrders();
				lifoCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
				break;
			case HIGH_COST:
				/**********************************************************************
				 * Process the HighCost Tax Lot Method
				 *********************************************************************/
				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.HIGH_COST);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "O7");						
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "O8");
				// mainExec.runTaxScenario(TestScenario.TAX_LOTS_DOC, openOrderTicket, closeOrderTicket);
				/**
				 * Query MongoDB openOrders collection to build openingOrders[]  
				 */
				daoOpeningOrder = new DaoOpeningOrder(openingOrderTicket, exec.mDbu);
				
				openingOrderTicket.setOpenOrdList(
						daoOpeningOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol, openingOrderTicket));

				/**
				 * Query MongoDB closingOrders collection to build closingOrders[]  
				 */
				daoClosingOrder = new DaoClosingOrder(closingOrderTicket, exec.mDbu);
				
				closingOrderTicket.setClosingOrderList(
						daoClosingOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol,closingOrderTicket));				
				
				CalcMethodHighCost highCostCalc = new CalcMethodHighCost(exec.ssClient, exec.mDbu);

				highCostCalc.setDumpItems(true);
				highCostCalc.setOpen( openingOrderTicket.getOpenOrdList());
				highCostCalc.setClose(closingOrderTicket.getClosingOrderList());

				highCostCalc.sortOrders();
				highCostCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
				break;
			case LONG_TERM_HIGH_COST:
				/**********************************************************************
				 * Process the LongTermHighCost Tax Lot Method
				 *********************************************************************/
				exec.currentTaxRate = exec.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
				exec.myTaxRate.setCapitalGainRates(exec.currentTaxRate.getIncomeRate());

				Transaction.setTaxLotMethodId(EnumTaxLotMethod.LONG_TERM_HIGH_COST);
				openingOrderTicket = new OpeningOrderTicket(exec.taxLotMethod, "O9");						
				closingOrderTicket = new ClosingOrderTicket(exec.taxLotMethod, "O10");

				//mainExec.runTaxScenario(TestScenario.TAX_LOTS_DOC, openOrderTicket, closeOrderTicket);
				/**
				 * Query MongoDB openOrders collection to build openingOrders[]  
				 */
				daoOpeningOrder = new DaoOpeningOrder(openingOrderTicket, exec.mDbu);
				
				openingOrderTicket.setOpenOrdList(
						daoOpeningOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol, openingOrderTicket));

				/**
				 * Query MongoDB closingOrders collection to build closingOrders[]  
				 */
				daoClosingOrder = new DaoClosingOrder(closingOrderTicket, exec.mDbu);
				
				closingOrderTicket.setClosingOrderList(
						daoClosingOrder.findByAccountIdAndSymbol(
							exec.accountId, exec.symbol,closingOrderTicket));				
				
				CalcMethodLongTermHighCost longTermHighCostCalc = 
						new CalcMethodLongTermHighCost(exec.ssClient, exec.mDbu);

				longTermHighCostCalc.setDumpItems(false);
				longTermHighCostCalc.setOpen( openingOrderTicket.getOpenOrdList());
				longTermHighCostCalc.setClose(closingOrderTicket.getClosingOrderList());

				longTermHighCostCalc.sortOrders();
				longTermHighCostCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
				break;
			case BEST_TAX:
				/**********************************************************************
				 * Process the Best Tax Lot Method
				 *********************************************************************/
				Transaction.setTaxLotMethodId(EnumTaxLotMethod.BEST_TAX);

				break;
			default:
				break;
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}

		Date endTime   = new Date();
		long end = endTime.getTime();
		System.out.println("Elapsed time: " + durationHMSm(end - start) + " seconds to complete" );
		System.out.println("\nTax Lot Methods mainExecutive Program Complete!");
	}
}
