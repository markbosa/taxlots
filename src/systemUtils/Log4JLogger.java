package systemUtils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
/**
 * @author Mark Bosakowski
 * Based on proven log4j 1.2.17
 */

public class Log4JLogger {
	private static org.apache.log4j.Logger log = Logger.getLogger(Log4JLogger.class);
	private String trace = "Trace";
	private String debug = "Debug";
	private String info  = "Info";
	private String warn  = "Warn";
	private String error = "Error";
	private String fatal = "Fatal";	

	/**
	 * Getter methods to retrieve the last message for each level
	 */
	/**
	 * @return the trace
	 */
	public String getTrace() {
		return trace;
	}
	/**
	 * @return the debug
	 */
	public String getDebug() {
		return debug;
	}
	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}
	/**
	 * @return the warn
	 */
	public String getWarn() {
		return warn;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @return the fatal
	 */
	public String getFatal() {
		return fatal;
	}
	/**
	 * Define a set of macro-like setter methods to simplify error logging
	 */
	/**
	 * @param log a trace level message
	 */
	public void Trace(String trace) {
		log.trace(trace);
		this.trace = trace;
	}
	/**
	 * @param log a debug level message
	 */
	public void Debug(String debug) {
		log.debug(debug);
		this.debug = debug;
	}
	/**
	 * @param log an info level message
	 */
	public void Info(String info) {
		log.info(info);
		this.info = info;
	}
	/**
	 * @param log a warning level message
	 */
	public void Warn(String warn) {
		log.warn(warn);
		this.warn = warn;
	}
	/**
	 * @param log a error level message
	 */
	public void Error(String error) {
		log.error(error);
		this.error = error;
	}
	/**
	 * @param log a fatal level message
	 */
	public void Fatal(String fatal) {
		log.fatal(fatal);
		this.fatal = fatal;
	}
	/**
	 * @return the log
	 */
	public static org.apache.log4j.Logger getLog() {
		return log;
	}

	/**
	 * @param log the log to set
	 */
	public static void setLog(org.apache.log4j.Logger log) {
		Log4JLogger.log = log;
	}

	public static void main(String[] args) {	
	    /**
	     * You MUST configure the logger with a valid properties file
	     */
        PropertyConfigurator.configure("C:\\Dev\\workspace\\TaxLot\\bin\\log4j.properties");
        
		log.trace("Trace");
		log.debug("Debug");
		log.info("Info");
		log.warn("Warn");
		log.error("Error");
		log.fatal("Fatal");
	}
}
