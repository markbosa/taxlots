package systemUtils;

import java.io.File;

//import java.lang.management.*;

public class SystemInfo {

	public boolean PrintSystemInfo() {
		// TODO Auto-generated constructor stub

		String sep = "================================================================================";
		System.out.println(sep);
		System.out.println("     " + "Hardware Information");
		System.out.println(sep);

		long maxMemory = Runtime.getRuntime().maxMemory();
		System.out.println("Available processors: " + Runtime.getRuntime().availableProcessors() + " cores");
		System.out.println("         Free memory: " + Runtime.getRuntime().freeMemory() + " bytes");
		System.out.println("      Maximum memory: " + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory) + " bytes");
		System.out.println("       Total memory : " + Runtime.getRuntime().totalMemory() + " bytes");

		/* Get a list of all file system roots on this system */
		File[] roots = File.listRoots();

		/* For each file system root, print some info */
		for (File root : roots) {
			System.out.println("");
			System.out.println("    File System root: " + root.getAbsolutePath());
			System.out.println("         Total space: " + root.getTotalSpace()  + " bytes");
			System.out.println("          Free space: " + root.getFreeSpace()   + " bytes");
			System.out.println("        Usable space: " + root.getUsableSpace() + " bytes");
		}
		System.out.println(sep);
		System.out.println("");

		System.out.println(sep);
		System.out.println("     Software Information");
		System.out.println(sep);

		System.out.println("");
		System.out.println(sep);
		System.out.println("     Java Virtual Machine (JVM) information");
		System.out.println(sep);
		System.out.println("JVM specification version : " + System.getProperty("java.vm.specification.version"));
		System.out.println("JVM specification vendor  : " + System.getProperty("java.vm.specification.vendor"));
		System.out.println("JVM specification name    : " + System.getProperty("java.vm.specification.name"));
		System.out.println("JVM implementation version: " + System.getProperty("java.vm.version"));
		System.out.println("JVM implementation vendor : " + System.getProperty("java.vm.vendor"));
		System.out.println("JVM implementation name   : " + System.getProperty("java.vm.name"));

		System.out.println("");
		System.out.println(sep);
		System.out.println("     Java Runtime Enviroment (JRE) information");
		System.out.println(sep);
		System.out.println("JRE version               : " + System.getProperty("java.version"));
		System.out.println("JRE vendor name           : " + System.getProperty("java.vendor"));
		System.out.println("JRE vendor url            : " + System.getProperty("java.vendor.url"));
		System.out.println("JRE specification version : " + System.getProperty("java.specification.version"));
		System.out.println("JRE specification vendor  : " + System.getProperty("java.specification.vendor"));
		System.out.println("JRE specification name    : " + System.getProperty("java.specification.name"));
		System.out.println("JRE installation direcotry: " + System.getProperty("java.home"));

		System.out.println("");
		System.out.println(sep);
		System.out.println("     Java Internals / Misc");
		System.out.println(sep);
		System.out.println("Java class format version number: " + System.getProperty("java.class.version"));
		System.out.println("Path of java class              : " + System.getProperty("java.class.path"));
		System.out.println("Search paths to load libraries  : " + System.getProperty("java.library.path"));
		System.out.println("The path of temp file           : " + System.getProperty("java.io.tmpdir"));
		System.out.println("The Name of JIT compiler        : " + System.getProperty("java.compiler"));
		System.out.println("Extension directory paths       : " + System.getProperty("java.ext.dirs"));

		System.out.println("");
		System.out.println(sep);
		System.out.println("     OS Specific Information");
		System.out.println(sep);
		System.out.println("The name of OS name: " + System.getProperty("os.name"));
		System.out.println("The OS architecture: " + System.getProperty("os.arch"));
		System.out.println("  The version of OS: " + System.getProperty("os.version"));
		System.out.println(" The File separator: " + System.getProperty("file.separator"));
		System.out.println(" The path separator: " + System.getProperty("path.separator"));
		System.out.println(" The line separator: " + System.getProperty("line.separator").codePointAt(0));

		System.out.println("");
		System.out.println(sep);
		System.out.println("      OS User Information");
		System.out.println(sep);
		System.out.println("Locale                   : " + System.getProperty("user.language"));
		System.out.println("Account name of user     : " + System.getProperty("user.name"));
		System.out.println("Home directory of user   : " + System.getProperty("user.home"));
		System.out.println("Current working directory: " + System.getProperty("user.dir"));

		System.out.println(sep);
		System.out.println(sep);
		return true;
	}


	/*	private boolean PerformanceMonitor { 
		private int  availableProcessors = getOperatingSystemMXBean().getAvailableProcessors();
		private int lastSystemTime      = 0;
		private int lastProcessCpuTime  = 0;

		public synchronized double getCpuUsage()
		{
			if ( lastSystemTime == 0 )
			{
				baselineCounters();
				return;
			}

			int systemTime     = System.nanoTime();
			int processCpuTime = 0;

			if ( getOperatingSystemMXBean() instanceof OperatingSystemMXBean )
			{
				processCpuTime = ( (OperatingSystemMXBean) getOperatingSystemMXBean() ).getProcessCpuTime();
			}

			double cpuUsage = (double) ( processCpuTime - lastProcessCpuTime ) / ( systemTime - lastSystemTime );

			lastSystemTime     = systemTime;
			lastProcessCpuTime = processCpuTime;

			return cpuUsage / availableProcessors;
		}

		private void baselineCounters()
		{
			lastSystemTime = System.nanoTime();

			if ( getOperatingSystemMXBean() instanceof OperatingSystemMXBean )
			{
				lastProcessCpuTime = ( (OperatingSystemMXBean) getOperatingSystemMXBean() ).getProcessCpuTime();
			}
		}
	}

	 */	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SystemInfo SysInf = new SystemInfo();
		SysInf.PrintSystemInfo();
	}

}
