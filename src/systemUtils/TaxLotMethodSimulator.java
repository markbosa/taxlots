package systemUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import calcEngine.CalcMethodFIFO;
import calcEngine.CalcMethodHighCost;
import calcEngine.CalcMethodLIFO;
import calcEngine.CalcMethodLongTermHighCost;
import calcEngine.EnumTaxLotMethod;
import calcEngine.EnumTaxRate;
import calcEngine.TaxRate;
import calcEngine.TaxRate.taxRateTier;
import orders.OpeningOrder;
import orders.OpeningOrderTicket;
import orders.ClosingOrder;
import orders.ClosingOrderTicket;
import orders.Transaction;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;

public class TaxLotMethodSimulator {
	long orderCnt = 1;
	String openOrderId = "";
	String closeOrderId = "";
	int transactionCnt = 1;
	TestScenario taxScenario = TestScenario.TAX_LOTS_DOC;
	TaxRate myTaxRate = new TaxRate();
	taxRateTier currentTaxRate = myTaxRate.getTaxRate()
					.get(EnumTaxRate.TAX_RATE_396.ordinal());
	private static EnumTaxLotMethod taxLotMethodId = 
				   EnumTaxLotMethod.NO_TAX_LOT_METHOD;
	SqlServerClient ssClient = null;
	MongoDBUtils mDbu = null;
	
	public TaxLotMethodSimulator() {		
	}

	private void runTaxScenario(TestScenario testCase, OpeningOrderTicket bot, ClosingOrderTicket sot) {
		System.out.println("Executing Test Scenario: " + testCase.name());
		switch(testCase) {
		case TAX_LOTS_DOC :			
			this.generateOpenOrder(bot, 1);
			this.generateOpenOrder(bot, 2);
			this.generateOpenOrder(bot, 3);
			this.generateOpenOrder(bot, 4);
			this.generateCloseOrder(sot, 1);
			this.generateCloseOrder(sot, 2);
			break;
		case RANDOM:
			break;
		case TAX_LOTS:
			break;
		case TAX_LOTS_INTRADAY:
			this.generateIntradayOpenOrder(bot);
			this.generateIntradayCloseOrder(sot);
			break;
		case TAX_LOTS_XLS:  // Order is Transaction T1->T5
			this.generateOpenOrder(bot, 1);
			this.generateOpenOrder(bot, 2);
			this.generateCloseOrder(sot, 4);
			this.generateOpenOrder(bot, 3);
			this.generateCloseOrder(sot, 5);
			break;
		default:
			break;
		}
	}
	
	/**
	 * generate test data for OpenOrders and CloseOrders
	 * switch(item) statement will generate one order at a time and add it
	 * to the Open Order Ticket, thus allowing us to generate multiple test
	 * scenarios off of the same set of data
	 */	
	private void generateOpenOrder(OpeningOrderTicket openOrderTckt, int item) {
		OpeningOrder bo = null;
		String corpActionType = "CA";
		
		switch(item) {
		case 1: 
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ",  
					setDateString(2013, 1, 1), 18, 
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());

			Transaction.setTaxLotMethodId(TaxLotMethodSimulator.taxLotMethodId);
			openOrderTckt.openOrdListAddOpen(bo);
			openOrderTckt.setOpeningOpenOrd(bo);
			break;
		case 2 :
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ",  
					setDateString(2013, 2, 1), 10, 
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);			
			break;
		case 3: 
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ", 
					setDateString(2013, 6, 1), 28,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);					
			break;
		case 4:
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ", 
					setDateString(2013, 7, 1), 20,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);		
			break;
		case 5:   	// T1 from XLS
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "IBM", 
					setDateString(2013, 1, 10), 10,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);		
			break;
			
		case 6:		// T2 from XLS
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "IBM", 
					setDateString(2013, 1, 10), 20,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);		
			break;
		case 7:	// T4 from XLS
			bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "IBM", 
					setDateString(2013, 1, 10), 25,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			openOrderTckt.openOrdListAddOpen(bo);		
			break;			
			
			
		default:
			break;
		}
	}

	private void generateCloseOrder(ClosingOrderTicket ordTx, int item) {
		String corpActionType = "CA-Split";

		switch( item ) {
		case 1: 
			ClosingOrder so1 = new ClosingOrder(Transaction.CLOSE, closeOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ", 
					setDateString(2014, 5, 1), 30,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			ordTx.closingOrderListAddClose(so1);
			break;
		case 2:
			ClosingOrder so2 = new ClosingOrder(Transaction.CLOSE, closeOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 100, "XYZ", 
					setDateString(2014, 5, 2), 30,
					myTaxRate.getShortTermRate(), 
					myTaxRate.getLongTermRate());
			ordTx.closingOrderListAddClose(so2);
			break;
		case 3:
			ClosingOrder so3 = new ClosingOrder(Transaction.CLOSE,  closeOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 30, "XYZ", 
					setDateString(2014, 5, 3), 30,
					myTaxRate.getShortTermRate(), myTaxRate.getLongTermRate());
			ordTx.closingOrderListAddClose(so3);
			break;
		case 4:		// O2:T3 from XLS
			ClosingOrder so4 = new ClosingOrder(Transaction.CLOSE,  closeOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 150, "IBM", 
					setDateString(2013, 2, 10), 20,
					myTaxRate.getShortTermRate(), myTaxRate.getLongTermRate());
			ordTx.closingOrderListAddClose(so4);
			break;
		case 5:		// O2:T5 from XLS
			ClosingOrder so5 = new ClosingOrder(Transaction.CLOSE,  closeOrderId, corpActionType,
					new Integer(transactionCnt++).toString(), 10, "IBM", 
					setDateString(2014, 5, 3), 20,
					myTaxRate.getShortTermRate(), myTaxRate.getLongTermRate());
			ordTx.closingOrderListAddClose(so5);
			break;
		}
	}

	/**
	 * generate test data for OpenOrders and CloseOrders for 2014
	 */	
	private void generateIntradayOpenOrder(OpeningOrderTicket ordTx) {
		String corpActionType = "CA";
		
		OpeningOrder bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 1), 18, 
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());

		Transaction.setTaxLotMethodId(TaxLotMethodSimulator.taxLotMethodId);
		ordTx.openOrdListAddOpen(bo);
		ordTx.setOpeningOpenOrd(bo);

		bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 2), 10, 
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.openOrdListAddOpen(bo);

		bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 3), 28,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.openOrdListAddOpen(bo);		

		bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 10), 20,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.openOrdListAddOpen(bo);	
		
		bo = new OpeningOrder(Transaction.OPEN, openOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 10), 20,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.openOrdListAddOpen(bo);		
	}

	private void generateIntradayCloseOrder(ClosingOrderTicket ordTx) {
		String corpActionType = "CA-I";

		ClosingOrder so1 = new ClosingOrder(Transaction.CLOSE,  closeOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 10), 30,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.closingOrderListAddClose(so1);

		ClosingOrder so2 = new ClosingOrder(Transaction.CLOSE, closeOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 10), 30,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.closingOrderListAddClose(so2);
		
		ClosingOrder so3 = new ClosingOrder(Transaction.CLOSE,  closeOrderId, corpActionType,
				new Integer(transactionCnt++).toString(), 100, "XYZ", 
				setDateString(2014, 1, 10), 30,
				myTaxRate.getShortTermRate(), 
				myTaxRate.getLongTermRate());
		ordTx.closingOrderListAddClose(so3);
	}
	
	
	public java.util.Date setDateString( int year, int month, int day) {

		String date = year + "/" + month + "/" + day;
		java.util.Date utilDate = null;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			utilDate = formatter.parse(date);
		//	System.out.println("utilDate:" + utilDate);
		} catch (ParseException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return utilDate;
	}

	
	public enum TestScenario {
		TAX_LOTS_DOC,
		TAX_LOTS_INTRADAY,
		TAX_LOTS_XLS,
		TAX_LOTS,RANDOM
	}
	/**
	 * The is the primary entry point into the proof-of-concept for TaxLot
	 * 
	 * Here are the steps you would follow to gather data and execute the 
	 * open and close orders against a tax method such as FIFO.  
	 * 
	 * Consider this to be the programmers API for generating tax lots
	 * @param args
	 */
	public static void main(String[] args) {
		/**
		 * Serially reused variables for testing only
		 * Expect distinct instance variable to derive from web services API
		 */
		OpeningOrderTicket   openingOrderTicket = null;
		ClosingOrderTicket closingOrderTicket = null;
				
		/**
		 * Create a new simulator class and Verify the taxRateTiers
		 * The setCapitalGainRates sets both the short and long term rates
		 * based upon the income rate of the equity owner
		 */
		TaxLotMethodSimulator tlms = new TaxLotMethodSimulator();
		
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());
		tlms.myTaxRate.printTaxRateTable();
	
		/**
		 * For each "Test Scenario" we want to populate the open and close orders
		 * with different open and close tickets.  The TAX_LOTS_DOC references 
		 * the documented test cases in the "LiquidTrade Tax Lots Support"
		 * document where the same data is used to illustrate all 6 tax methods
		 * 
		 * The TAX_LOTS_XLS pre-populated data to match the test scenarios
		 * found in the "TaxLotScenarios.xls" spreadsheet.
		 * 
		 * The TAX_LOTS_INTRADAY modifies the tests to run the INTRADAY sort
		 * option followed by FIFO. 
		 * 
		 * TAX_LOTS - TBD
		 * RANDOM - Meant for a stress test where we would generate random 
		 * transactions with random values for QA.
		 */
		
		/**********************************************************************
		 * Test 1:  The FIFO Tax Lot Method
		 * We need to generate new Tax Lot Data and clear old data
		 *********************************************************************/	
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());
	
		taxLotMethodId = EnumTaxLotMethod.FIFO;
		tlms.openOrderId = "O1";
		tlms.closeOrderId = "02"; 
		openingOrderTicket = new OpeningOrderTicket(taxLotMethodId, tlms.openOrderId);
		closingOrderTicket = new ClosingOrderTicket(taxLotMethodId, tlms.closeOrderId);
		tlms.runTaxScenario(TestScenario.TAX_LOTS_DOC, openingOrderTicket, closingOrderTicket);
		
		CalcMethodFIFO fifoCalc = new CalcMethodFIFO(tlms.ssClient, tlms.mDbu);

		fifoCalc.setDumpItems(true);

		fifoCalc.setOpen(openingOrderTicket.getOpenOrdList());
		fifoCalc.setClose(closingOrderTicket.getClosingOrderList());
		
		fifoCalc.sortOrders();
		fifoCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
		fifoCalc.verifyByCrossTab("Main(FIFO)");
		
		/**********************************************************************
		 * Test 2:  The FIFO Intraday Tax Lot Method
		 * We need to generate new Tax Lot Data and clear old data
		 *********************************************************************/	
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());
	
		taxLotMethodId = EnumTaxLotMethod.FIFO;
		tlms.openOrderId = "O3";
		tlms.closeOrderId = "04"; 
		openingOrderTicket = new OpeningOrderTicket(taxLotMethodId, tlms.openOrderId);						
		closingOrderTicket = new ClosingOrderTicket(taxLotMethodId, tlms.closeOrderId);
		tlms.runTaxScenario(TestScenario.TAX_LOTS_INTRADAY, openingOrderTicket, closingOrderTicket);

		/**		
		 * set the Intraday option to post-modify the FIFO Sort
		 */
		CalcMethodFIFO fifoCalcIntraday = new CalcMethodFIFO(tlms.ssClient, tlms.mDbu);

		fifoCalcIntraday.setIntraday(true);
		fifoCalcIntraday.setDumpItems(true);

		fifoCalcIntraday.setOpen(openingOrderTicket.getOpenOrdList());
		fifoCalcIntraday.setClose(closingOrderTicket.getClosingOrderList());
		
		fifoCalcIntraday.sortOrders();
		fifoCalcIntraday.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
		fifoCalcIntraday.verifyByCrossTab("Main(FIFO Intraday)");
			
		/**********************************************************************
		 * Test 3: The LIFO Method
		 * Get simulated input data assigning each Tax Lot Method in each item 
		 *********************************************************************/
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());
	
		taxLotMethodId = EnumTaxLotMethod.LIFO;
		tlms.openOrderId = "O5";
		tlms.closeOrderId = "06"; 
		openingOrderTicket = new OpeningOrderTicket(taxLotMethodId, tlms.openOrderId);						
		closingOrderTicket = new ClosingOrderTicket(taxLotMethodId, tlms.closeOrderId);
		tlms.runTaxScenario(TestScenario.TAX_LOTS_DOC, openingOrderTicket, closingOrderTicket);
	
		CalcMethodLIFO lifoCalc = new CalcMethodLIFO(tlms.ssClient, tlms.mDbu);	
		
		lifoCalc.setDumpItems(false);
		lifoCalc.setOpen(openingOrderTicket.getOpenOrdList());
		lifoCalc.setClose(closingOrderTicket.getClosingOrderList());

		lifoCalc.sortOrders();
		lifoCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
		lifoCalc.verifyByCrossTab("Main(LIFO)");

		
		/**********************************************************************
		 * Test 4: The HighCost Tax Lot Method
		 * We need to generate new Tax Lot Data and clear old data
		 *********************************************************************/
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_396.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());

		
		taxLotMethodId = EnumTaxLotMethod.HIGH_COST;
		tlms.openOrderId = "O7";
		tlms.closeOrderId = "08"; 
		openingOrderTicket = new OpeningOrderTicket(taxLotMethodId, tlms.openOrderId);						
		closingOrderTicket = new ClosingOrderTicket(taxLotMethodId, tlms.closeOrderId);
		tlms.runTaxScenario(TestScenario.TAX_LOTS_DOC, openingOrderTicket, closingOrderTicket);
		
		CalcMethodHighCost highCostCalc = new CalcMethodHighCost(tlms.ssClient, tlms.mDbu);

		highCostCalc.setDumpItems(true);
		highCostCalc.setOpen(openingOrderTicket.getOpenOrdList());
		highCostCalc.setClose(closingOrderTicket.getClosingOrderList());
		
		highCostCalc.sortOrders();
		highCostCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
		highCostCalc.verifyByCrossTab("Main(HIGH_COST)");
				
		/**********************************************************************
		 * Test 5: The LongTermHighCost Tax Lot Method
		 * We need to generate new Tax Lot Data
		 *********************************************************************/
		tlms.currentTaxRate = tlms.myTaxRate.getTaxRate().get(EnumTaxRate.TAX_RATE_28.ordinal());
		tlms.myTaxRate.setCapitalGainRates(tlms.currentTaxRate.getIncomeRate());
	
		taxLotMethodId = EnumTaxLotMethod.LONG_TERM_HIGH_COST;
		tlms.openOrderId = "O9";
		tlms.closeOrderId = "10"; 
		openingOrderTicket  = new OpeningOrderTicket( taxLotMethodId, tlms.openOrderId);						
		closingOrderTicket = new ClosingOrderTicket(taxLotMethodId, tlms.closeOrderId);
		
		tlms.runTaxScenario(TestScenario.TAX_LOTS_DOC, openingOrderTicket, closingOrderTicket);

		CalcMethodLongTermHighCost longTermHighCostCalc = new CalcMethodLongTermHighCost(tlms.ssClient, tlms.mDbu);

		longTermHighCostCalc.setDumpItems(false);
		longTermHighCostCalc.setOpen(openingOrderTicket.getOpenOrdList());
		longTermHighCostCalc.setClose(closingOrderTicket.getClosingOrderList());

		longTermHighCostCalc.sortOrders();
		longTermHighCostCalc.calculateTaxMethod(openingOrderTicket, closingOrderTicket);
		longTermHighCostCalc.verifyByCrossTab("Main(LONG_TERM_HIGH_COST)");

		System.out.println("\nTaxLotMethodSimulator Program Complete!");
	}
}
