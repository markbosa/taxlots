/**
 * 
 */
package orders;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * ItradayOrder() is a class that selectively cherry-picks open lot items
 *               with the intention of processing same day(aka Intraday) order.
 * 
 * Usually, Intraday FIFO is used together with regular FIFO, but it can 
 * actually be used in conjunction with any Standard Tax Lot Relief Method, 
 * which would kick in after the Intraday “situations” have been taken care of.

Intraday FIFO option is commonly used for Futures.
 * @author Mark Bosakowski
 *
 */
public class IntradayOrder {
	LinkedList<OpeningOrder> open = null;
	/**
	 * Constructor passes in the list to be manipulated
	 * This leaves the design open for custom Intraday processing, however
	 * it is expected that the basic algorithm will take a pre-sorted list
	 * (based on Tax Lot Method) and to simply move the matching days to
	 * the head of the list.
	 */
	public IntradayOrder(LinkedList<OpeningOrder> openList) {
		this.open = openList;
	}

	/**
	 * For each close in the closeList, parse through the openList items and any 
	 * open date that matches the close date will be moved to the top of the 
	 * open list to be processed first.
	 * 
	 * @param openList
	 * @param closeList
	 */
	public void IntradaySort(
			LinkedList<OpeningOrder>  openList, 
			LinkedList<ClosingOrder> closeList) {
		
		/**
		 *  Outer loop only processes 1 close order as we assume that 
		 *  by definition, all closes are on the same day.  Validation
		 *  would have caught this condition and prevented this code from
		 *  executing, closeOrd we can assume at this point that all all close 
		 *  orders are on the same day. 
		 */
		Iterator<ClosingOrder> closeIt = closeList.iterator();
		
		if(closeIt.hasNext()) {
			ClosingOrder tliClose = closeIt.next();
			
			/**
			 *  Inner loop - Calculate Tax on OPEN Orders by Date < or > 1 year
			 */
			int compare = 0;
			int headItem = 0;
			
			for(int i = 0; i < openList.size(); i++ ) {

				OpeningOrder tliOpen = openList.get(i);

				compare = tliClose.compareDates(tliOpen, tliClose);
				
				/**
				 * If the  Close Date is the same as the Open Date then we 
				 * need to move the open item to the top of the list
				 */
				if( compare == 0) {
					openList.remove(i);
					openList.add(headItem++,tliOpen);
				}
			}
		}
	}
}
