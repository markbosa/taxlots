/**
 * 
 */
package orders;

import java.util.Iterator;
import java.util.LinkedList;

import persistence.DaoOpenTaxLot;
import persistence.DaoOpeningOrder;
import persistence.MongoDBUtils;
import calcEngine.EnumTaxLotMethod;
import calcEngine.TaxLot;
import calcEngine.TaxLotBucket;

/**
 * The requirements document "TaxLotScenarios.xls" describes a relationship
 * between an OpeningOrderTicket and Transactions, which could be OPEN and CLOSE types.
 * 
 * 
 * The Parent OrderTicket will manage the Closing transactions in the 
 * ClosingOrderTicket, along with the Opening transactions in the OpeningOrderTicket
 * 
 * @author Mark
 *
 */
public class OpeningOrderTicket extends OrderTicket {
	private OpeningOrder openingOpenOrd = null;
	LinkedList<OpeningOrder>  openList = new LinkedList<OpeningOrder>();
	TaxLot currentTaxLot = null;
	TaxLotBucket taxLotBucket = new TaxLotBucket();
	private int taxLotId = 1;

	/**
	 * Constructor captures tax lot method and the order ticket  
	 */
	public OpeningOrderTicket(EnumTaxLotMethod openOrdLtMethodId, String ticket) {
		super(ticket, openOrdLtMethodId);
	}
	
		
	public void openOrdListAddOpen(OpeningOrder newOpenOrd) {
		this.openList.add(newOpenOrd);
	}


	public LinkedList<OpeningOrder> getOpenOrdList() {
		return openList;
	}


	public void setOpenOrdList(LinkedList<OpeningOrder> byList) {
		this.openList = byList;
	}


	/**
	 * Getter and Setter methods for OpeningOrderTicket
	 */
	public OpeningOrder getOpeningOpenOrd() {
		return openingOpenOrd;
	}


	public void setOpeningOpenOrd(OpeningOrder openingOpenOrd) {
		this.openingOpenOrd = openingOpenOrd;
	}

	
	/**
	 * OpenOrderEtl - Open Order ETL Initial Data Loading
	 * 
	 * To handle the situation where there are Open open orders we have to 
	 * extract the open trade data using the DaoTransactions class and then
	 * transform the data into an Open Position and persist it as an 
	 * Open Tax Lot 
	 * @param ssClient 
	 */
	public void openOrderEtl(MongoDBUtils mDbu, OpeningOrderTicket openOrderTicket) {
		/**
		 *  Perform ETL on Open Orders 
		 */
		DaoOpeningOrder doo = new DaoOpeningOrder(openOrderTicket, mDbu);
		Iterator<OpeningOrder>  openIt = openList.iterator();	

		while(openIt.hasNext()) {

			OpeningOrder tliOpen = openIt.next();
			
			/**
			 * Now insert the closing orders one at a time
			 */
			doo.insertOpeningOrder(tliOpen);
			/**
			 * If we are processing a OpeningOrder for the first time, then
			 * we need to create a TaxLot object to tie the Open to the
			 * Close Order.   Increment TaxLotId until we get final transaction numbers from database
			 */
			this.currentTaxLot = new TaxLot(openOrderTicket, tliOpen);
		//	this.currentTaxLot.setLotId("TL" + new Integer(taxLotId ++).toString());
			this.currentTaxLot.setLotId(++taxLotId);
			this.taxLotBucket.addTaxLot(currentTaxLot);
	
			/**
			 * Now insert the current tax lot one at a time
			 */
//			DaoSqlTaxLot daoTaxLot = new DaoSqlTaxLot(ssClient);

			DaoOpenTaxLot daoOpenTaxLot = new DaoOpenTaxLot(mDbu);
			daoOpenTaxLot.insertTaxLot(this.getHostOrderId(), this.currentTaxLot);
		}		
	}
	/**
	 *  The OpeningOrderTicket contains a list of 1 or more OPEN Orders
	 *  Print out the OpeningOrderTicket then iterate and print Open Order details
	 */
	public void printOpenOrders() {
		Iterator<OpeningOrder>  openIt = openList.iterator();	

		while(openIt.hasNext()) {

			OpeningOrder open = openIt.next();
			
			open.printSubData(true);
		}
	}
	
	/**
	 * Print out the details of a single OpeningOrder
	 */
	public void printOpenOrder(OpeningOrder  openingOrder) {	
			openingOrder.printSubData(true);
	}
}
