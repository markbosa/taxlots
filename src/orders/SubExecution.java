/**
 * 
 */
package orders;

import java.text.DecimalFormat;

/**
 * Sub Executions - A 1 to many relationship between a CLOSE order and the
 * number of executions against OPEN orders required to fulfill the CLOSE order
 * 
 * The OPEN OrderTicket contains the original number of shares bought
 * The CLOSE OrderTicket contains a request to close a number of shares that may be
 *   1) The EXACT number of shares in a known OPEN order (100 open = 100 close)
 *   2) A Subset of a OPEN order (50 close from a 100 open order)
 *   3) A Close OrderTicket that requires two or more OPEN orders to fulfill
 *         CLOSE 500 from 5 Open Orders of 100 each
 *         
 *   The tax rate is dependent on the OPEN date, closeOrd ONE CLOSE order may have
 *   both long and short term taxes.  Splitting one CLOSE order into instances
 *   allows us to track how many shares were sold at which rate.
 *   
 * @author Mark
 *
 */
public class SubExecution {
	private int openQuantity = 0;		// number of sub-shares against open order
	private int closedQuantity = 0;		// number of sub-shares closed out
	private double taxAmount = 0.0;		// dollar amount of sub-transaction
	private String taxRate = "";		// Short Term or Long Term
	private String txId = "";			// Temporary transactionID generated in constructor
	
	
	/**
	 * One stop constructor
	 */
	public SubExecution(int openQnt, int closedQnt, double amount, String rate) {
		this.openQuantity = openQnt;
		this.closedQuantity = closedQnt;
		this.taxAmount   = amount;
		this.taxRate  = rate;
		
		/**
		 * Until we resolve how to get a transaction ID, we will use a large
		 * string with the key data concatenated to produce a semi-unique ID
		 */

		/**
		 * eliminate roundoff digits first and limit to two decimal places
		 */
		DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(2);
        
		this.setTxId(getTxId().concat(new Long(this.openQuantity).toString()).concat("/")
				.concat(new Long(this.closedQuantity).toString())
				.concat(" taxed $")
				.concat(df.format(this.taxAmount))
				.concat("@")
				.concat(taxRate));
	}


	/**
	 * Getter and Setter methods
	 */
	public int getSubQuantity() {
		return openQuantity;
	}


	public void setSubQuantity(int subQuantity) {
		this.openQuantity = subQuantity;
	}


	public double getSubAmount() {
		return taxAmount;
	}


	public void setSubAmount(double subAmount) {
		this.taxAmount = subAmount;
	}


	public String getSubTaxRate() {
		return taxRate;
	}


	public void setSubTaxRate(String subTaxRate) {
		this.taxRate = subTaxRate;
	}


	public String getTxId() {
		return txId;
	}


	public void setTxId(String txId) {
		this.txId = txId;
	}
}
