package orders;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

/**
 * Three levels
 * 1) Position  MSFT at 
 * 2) TaxLots (Open and Closed)
 * 3) Tax Lot Closure
 * 
 * ClosingOrders have a "rank" to assist in the more complicated tax lot 
 * methods.  i.e. LongTermHighCost has attributes that are assigned, and
 * Cumulative properties are used to determine the rank.  
 * @author Mark
 */
public class ClosingOrder extends Transaction {
	static String shortTerm = "Short Term";
	static String longTerm = "Long Term";
	private String taxRateString = "Term Not Set";
	private ArrayList<SubExecution> sel = new ArrayList<SubExecution>();
	private int rank = -1;

	public ClosingOrder() {
	}

	public ClosingOrder(boolean openClose, String ordId, String tradeId, String corpActionType, 
			int qnt, String sym, Date newDate,
			double prc, double stRate, double ltRate) {
		super(openClose, ordId, tradeId, corpActionType, qnt, sym, newDate, prc, stRate, ltRate);
	}

	public ClosingOrder(boolean openClose, String ordId, String tradeId, String corpActionType, 
			int quantity, String instrument, Date date, double price) {
		super(openClose, ordId, tradeId, corpActionType, quantity, instrument, date, price, 0.0, 0.0);
	}
	
	/**
	 * Add any SubExecution classes to this ClosingOrder
	 * @param subX - A populated instance of a SubExecution class
	 */
	public void addToCloseOrder(SubExecution subX) {
		this.sel.add(subX);
	}

	/**
	 * For validation and/or logging, we print out the subExecutions
	 * that are part of this ClosingOrder instance.
	 */
	public void printSubExecutions() {

		Iterator<SubExecution>  subExIt = ((List<SubExecution>) this.sel).iterator();	

		if(subExIt.hasNext() == false)
			return;
		
		DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(2);
        		
		while(subExIt.hasNext()) {
			SubExecution subEx = subExIt.next();
			System.out.println(
					"\tClose OrderTicket: " + this.getTradeId() +
					" fulfilled by Sub Execution ID: " + subEx.getTxId() +			
					" \tat " +  subEx.getSubTaxRate() + " tax of $" +
					 df.format(subEx.getSubAmount()));
		}
		System.out.println("");
	}
	
	/**
	 * Getter / Setter methods
	 */
	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
	/**
	 * An OpeningOrderTicket will Match one or more close orders and each 
	 * closing taxRateString will be set to short or long term.  
	 * @return current setting of taxRateString variable
	 */
	public String getTaxRateString() {
		return taxRateString;
	}

	public void setTaxRateString(String taxRateStr) {
		this.taxRateString = taxRateStr;
	}


	/**
	 * Compare method for two Close Dates 
	 * 
	 * @param tliClose1 - A Close OrderTicket 
	 * @param tliClose2 - The next Close OrderTicket
	 * @return 0 if dates are the same
	 * 		  -1 if close1 before close2
	 *         1 if close1 after  close2
	 */
	public int compareCloseDates(ClosingOrder tliClose2) {
		/**
		 *  Normalize Open Date & Close Date 
		 */
		GregorianCalendar close1CalDate = new GregorianCalendar();
		close1CalDate.setTime(this.getDate());

		GregorianCalendar close2CalDate = new GregorianCalendar();
		close2CalDate.setTime(tliClose2.getDate());
		
		if(close1CalDate.compareTo(close2CalDate) == 0) {
			return 0;
		}
		else if(close1CalDate.before(close2CalDate)){
			return -1;
		}
		else if(close1CalDate.after(close2CalDate)){
			return 1;
		}
		else
			return -999;
	}

	@Override
	public void printSubData(boolean details) {
		if(details == true)
			this.printSubExecutions();
	}
}
