package orders;

import calcEngine.EnumTaxLotMethod;
/**
 * A thin base class that allows children to implement financial symbol
 * behavior, but still have a common place for method Id and Ticket number.
 * @author Mark
 */
public class OrderTicket {
	protected EnumTaxLotMethod taxLotMethodId = EnumTaxLotMethod.NO_TAX_LOT_METHOD;
	private String hostOrderId = "";
	
	public OrderTicket(String newTicket, EnumTaxLotMethod txLtMethodId) {
		this.setHostOrderId(newTicket);
		this.taxLotMethodId = txLtMethodId;
	}

	public String getHostOrderId() {
		return hostOrderId;
	}

	public void setHostOrderId(String ticket) {
		this.hostOrderId = ticket;
	}
}
