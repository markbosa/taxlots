/**
 * 
 */
package orders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import calcEngine.EnumTaxLotMethod;

/**
 * @author Mark Bosakowski
 *
 * An abstract class to encapsulate the common line entry data for one item
 * The intention is to share common data, but allow for symbol specific
 * behavior and properties by the child class.  This allows us to create unique
 * behavior based on class for each tax lot method calculation.
 * 
 * Examples: CLOSE, CLOSE FUTURE, OPTION, etc.
 */
public abstract class Transaction implements Comparable<Date>  {
	/**
	 * For faster access use protected vs. getter/setter methods
	 */
	private static EnumTaxLotMethod taxLotMethodId = EnumTaxLotMethod.NO_TAX_LOT_METHOD;

	private String hostOrderId = "";
	private String accountId = "";
	private String tradeId = "";
	private String exchangeMIC = "";
	private String marketMIC = "";
	private String assetType = "";
	private String tradeText = "";
	private String corporateActionType = "";

	private int quantity = 0;
	private int balance = 0;
	private double price = 0.0;
	private double taxAmount = 0.0;
	private double orderValue = 0.0;
	private String currency = "";
	private String symbol = "";
	private String isin = "";

	private double realizedGain = 0.0;
	private double unrealizedGain = 0.0;
	private double shortTermTaxRate = 0.0;
	private double longTermTaxRate = 0.0;
	private Date date = new Date();
	
	public static boolean OPEN = true;
	public static boolean CLOSE = false;
	boolean open = OPEN; 
	public TaxLotStatus status = TaxLotStatus.LOT_ITEM_OPEN;

	public enum TaxLotStatus {
		LOT_ITEM_OPEN,
		LOT_ITEM_CLOSED,
		LOT_ITEM_PARTIAL_CLOSE,
		LOT_OPEN,
		LOT_OPEN_CROSSING_ZERO,
		LOT_CLOSED,
		LOT_PARTIAL_CLOSE;

		private int value = 0;

		protected void setStatusValue(int value) {
			this.value = value;
		}

		public int getStatusValue() {
			return value;
		}
	}
	
	/**
	 * default constructor
	 */
	Transaction() {		
	}
	
	/**
	 * For simplicity of data input, the parameter order matches the 
	 * requirement documentation as in: 
	 * 
	 * Open 100 shares of XYZ on 02/01/2013 @ $10 per share
	 *  
	 * @param qnt	- Quantity (i.e. 100 Stock shares)
	 * @param sym	- Symbol from Symbology (i.e. IBM, AAP)
	 * @param date	- The execution date of the open or close order
	 * @param prc	- The price of the item at execution time
	 */
	Transaction(boolean openClose, String ordId, String trdId,
			String corpActionType, int qnt, String sym, 
			Date newDate, double prc, double stRate, double ltRate) {
		this.open = openClose;
		this.setHostOrderId(ordId);
		this.setCorporateActionType(corpActionType);
		this.setQuantity(qnt);
		this.setPrice(prc);
		this.setOrderValue(qnt * prc);  // A derived value
		this.setSymbol(sym);
		this.setDate(newDate);
		this.setShortTermTaxRate(stRate);
		this.setLongTermTaxRate(ltRate);
		this.setTradeId(trdId);
		
		/**
		 * Set balance based on open or closing order 
		 */
		if(this.open == true) {
			this.setBalance(qnt);
		}
		else {
			setBalance(0); 
		}		
	}
	
	public double getRealizedGain() {
		return this.realizedGain;
	}

	public void setRealizedGain(double realizedGain) {
		this.realizedGain = realizedGain;
	}

	/**
	 * Each ClosingTaxLot Item will have its own "Realized Gain" value.
	 * The total Tax Lot total Realized Gain will be the sum of the 
	 * individual realized gains closingOrder in the cases where there are
	 * 2 or more closing orders to close an opening tax order. 
	 * 
	 * @param open  - The opening order
	 * @param close - The closing order 
	 * @return The realized gain from the sale of the closing order vs.
	 *         the price of the opening order 
	 */
	public double calculateRealizedGain( 
			Transaction open, 
			Transaction close) {
		
		double tmpGain = (close.getPrice() - open.getPrice()) * close.getBalance();
		this.setRealizedGain(tmpGain);   // FRACTIONAL FIX GOES HERE! 
		return tmpGain;	
	}
	
	/**
	 * calculateTaxAmount(open, close)  A method to apply the Tax Lot Items
	 * tax rate against the "realized gain" from the sale.  The Tax Rate will
	 * be either a short term or a long term tax rate which is determined if 
	 * the security was held for 1 year(Long Term) or less(Short Term).
	 * 
	 * Since OPEN --> CLOSED is a 1 to Many relationship, it may take 2 or more
	 * OPEN orders to fulfill the closing of the OPEN order, closeOrd we MUST tabulate
	 * the sum of all taxable CLOSED Orders against this OPEN.
	 * 
	 * @return taxable amount for THIS open/close transaction only 
	 */
	public double calculateTaxAmount(OpeningOrder tliOpen, ClosingOrder tliClose) {
		double newTaxAmount = 0.0;

		/**
		 *  Open Date - If 1 year older than close date, then we will use
		 *  the Long Term Tax rate which is lower than short term rate	
		 */
		GregorianCalendar openCalDate = new GregorianCalendar();
		openCalDate.setTime(tliOpen.getDate());
		
		/**
		 *  Close Date - Subtract 1 year for short-term vs. long-term rate test
		 */
		GregorianCalendar closeCalDate = new GregorianCalendar();
		closeCalDate.setTime(tliClose.getDate());
		
		/**
		 * Short Term Date (std) minus one year for applied rate calculation
		 */
		GregorianCalendar std = new GregorianCalendar();
		std.setLenient(false);
		std.set(GregorianCalendar.YEAR, (closeCalDate.get(GregorianCalendar.YEAR)-1));
		std.set(GregorianCalendar.MONTH, closeCalDate.get(GregorianCalendar.MONTH));
		std.set(GregorianCalendar.DATE,  closeCalDate.get(GregorianCalendar.DAY_OF_MONTH));

		if(openCalDate.before(std)){
			tliClose.setTaxRateString(ClosingOrder.longTerm);
			
	        System.out.println("\tThe " + closeCalDate.getTime() + " Close will be done against the "
	                             + openCalDate.getTime()  + " Open lot." );
	  
	        newTaxAmount = this.realizedGain * this.longTermTaxRate;

	        System.out.printf("\t\tRealized Gain = Closed %d shares @ $%.2f against Open @ $%.2f per share = $%.2f\n", 
	        		tliClose.getBalance(), tliClose.getPrice(), tliOpen.getPrice(), tliOpen.getRealizedGain());
	        
	        System.out.printf(
		        	"\t\tTax = Realized Gain * Long Term Tax Rate of = $%.2f * %.01f%% = $%.2f\n",
		        	this.realizedGain, (this.longTermTaxRate * 100), newTaxAmount);
	      }
		
		
		if(openCalDate.after(std)){
			tliClose.setTaxRateString(ClosingOrder.shortTerm);
			
	        System.out.println("\tOpen Date " + openCalDate.getTime() 
					+ "  AFTER Close Date (minus 1 year) : "+ std.getTime() );

	        newTaxAmount = this.realizedGain * this.getShortTermTaxRate();
	      
	        System.out.printf("\t\tRealized Gain = Closed %d shares @ $%.3f against Open @ $%.3f per share = $%.2f\n", 
	        		tliClose.getBalance(), tliClose.getPrice(), tliOpen.getPrice(), tliClose.getRealizedGain());
   	        
	        System.out.printf(
		        	"\t\t\tTax = Realized Gain * Short Term Tax Rate = $%.3f *  %.01f%% = $%.2f\n",
		        	this.realizedGain, (this.getShortTermTaxRate() * 100), newTaxAmount);		
		}
			
        System.out.println();

		/**
		 * Because a OPEN order may take several CLOSED orders to CLOSE, 
		 * we must keep a running tally on the total TAX applied to the OPEN
		 */
		this.setTaxAmount(newTaxAmount);   // XXXX Change tally to Lot level, not Open
		return newTaxAmount;
	}
	
	/**
	 * compareDates() - A utility method to determine if a date is one of 
	 * 3 states:
	 * 		1) Open date is the same as close date
	 * 		2) Open date is earlier than close date
	 * 		3) Open date is later than close date (probably invalid)
	 * 
	 * This is located in this class to be called by all instruments that 
	 * derive from this class
	 * 
	 * @param tliOpen	- Open OrderTicket
	 * @param tliClose	- Close OrderTicket
	 * @return -1 if Open before Close date, 0 if date is the same or 1 if the
	 * 				Open is after Close date;  Fatal returns -999
	 */
	public int compareDates(OpeningOrder tliOpen, ClosingOrder tliClose) {
		/**
		 *  Normalize Open Date & Close Date 
		 */
		GregorianCalendar openCalDate = new GregorianCalendar();
		openCalDate.setTime(tliOpen.getDate());

		GregorianCalendar closeCalDate = new GregorianCalendar();
		closeCalDate.setTime(tliClose.getDate());
		
		if(openCalDate.compareTo(closeCalDate) == 0) {
			return 0;
		}
		else if(openCalDate.before(closeCalDate)){
			return -1;
		}
		else if(openCalDate.after(closeCalDate)){
			return 1;
		}
		else
			return -999;
	}

	
	/**
	 * Diagnostic method for testing
	 */
	public void printTaxLotItem() {
		String lotStatus = "";
		int displayMode = 1;  // 1 = linear 2 = stacked

		switch(status) {
		case LOT_ITEM_OPEN :
			lotStatus = "LI_OPEN";
			break;
		case LOT_ITEM_CLOSED :
			lotStatus = "LI_CLOSED";
			break;
		case LOT_ITEM_PARTIAL_CLOSE:
			lotStatus = "LI_PARTIAL_CLOSE";
			break;
		case LOT_CLOSED:
		case LOT_OPEN:
		case LOT_PARTIAL_CLOSE:
			lotStatus = status.name();
			break;
		default:
			break;
		}

		DateFormat df = new SimpleDateFormat("MM/dd/yy");
		String formattedDate = df.format(getDate()); 

		double dollarAmount = 0.0;

		if(open == true)
			dollarAmount = getPrice() * getQuantity();
		else
			dollarAmount = getPrice() * getBalance();

		if(displayMode == 1) {
			System.out.printf(
					"%s Quantity: %5d \t\tBalance: %5d     Status: %13s \tSymbol: %5s  tradeId: %s Date: %s  Price: %4.3f%n", 
					(open == true ? "Open  " : "Close "), getQuantity(), getBalance(), 
					lotStatus, getSymbol(), getTradeId(), formattedDate,  getPrice());
			if(this.status != TaxLotStatus.LOT_ITEM_OPEN)
				System.out.printf( "     TX Amount: $%5.2f \tRealized Gain: $%5.2f \tTaxable Amount: $%5.2f%n",  
									dollarAmount, this.getRealizedGain(), this.getTaxAmount(), dollarAmount);
		}
		else {
			System.out.printf(
					"%s Quantity: %d %n\tBalance: %d  %n\tStatus: %s %n\tSymbol: %s %n\tDate: %s  %n\tPrice: %4.3f%n", 
					(open == true ? "Open" : "Close"), getQuantity(), getBalance(), 
					lotStatus, getSymbol(), formattedDate,  getPrice());

			if(this.status != TaxLotStatus.LOT_ITEM_OPEN)
				System.out.printf( 
				"\tTX Amount: $%5.2f  %n\tRealized Gain: $%5.2f %n\tTaxable Amount: $%5.2f%n%n",  
				dollarAmount, this.getRealizedGain(), this.getTaxAmount(), dollarAmount);
		}
		
		this.printSubData(true);
	}
	

	public abstract void printSubData(boolean detailFlag);
	
	@Override
	public int compareTo(Date d) {
		System.out.println("STUB: compareTo(Date d)"  );
		return 0;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static EnumTaxLotMethod getTaxLotMethodId() {
		return taxLotMethodId;
	}

	public static void setTaxLotMethodId(EnumTaxLotMethod taxLotMethodId) {
		Transaction.taxLotMethodId = taxLotMethodId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency2) {
		this.currency = currency2;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String instrument) {
		this.symbol = instrument;
	}

	public double getLongTermTaxRate() {
		return longTermTaxRate;
	}

	public void setLongTermTaxRate(double longTrmTaxRate) {
		this.longTermTaxRate = longTrmTaxRate;
	}
	
	public double getShortTermTaxRate() {
		return shortTermTaxRate;
	}

	public void setShortTermTaxRate(double shortTrmTaxRate) {
		this.shortTermTaxRate = shortTrmTaxRate;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	public String getCorporateActionType() {
		return corporateActionType;
	}

	public void setCorporateActionType(String corporateActionType) {
		this.corporateActionType = corporateActionType;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	
	public String getHostOrderId() {
		return hostOrderId;
	}

	public void setHostOrderId(String orderId) {
		this.hostOrderId = orderId;
	}

	public String getExchangeMIC() {
		return exchangeMIC;
	}

	public void setExchangeMIC(String exchangeMIC) {
		/**
		 * In the SQL Server query, this field is a fixed 15 characters
		 * usually trailed by zero's.  Trim it for now
		 */
		this.exchangeMIC = exchangeMIC.trim();
	}

	public String getMarketMIC() {
		return marketMIC;
	}

	public void setMarketMIC(String marketMIC) {
		this.marketMIC = marketMIC;
	}

	public String getTradeText() {
		return tradeText;
	}

	public double getUnrealizedGain() {
		return unrealizedGain;
	}

	public void setUnrealizedGain(double unrealizedGain) {
		this.unrealizedGain = unrealizedGain;
	}

	public void setTradeText(String tradeText) {
		this.tradeText = tradeText;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date2) {
		this.date = date2;
	}

	public double getOrderValue() {
		return orderValue;
	}

	public void setOrderValue(double orderValue) {
		this.orderValue = orderValue;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	/**
	 * Multiple Comparators are a key design pattern to allow for sorting open 
	 * and close orders on different criteria, such as date, price, quantity
	 */
	public static Comparator<Transaction> TaxLotOpeningComparator = new Comparator<Transaction>() {

		public int compare(Transaction s1, Transaction s2) {
//			 System.out.println("STUB:: TaxLotItemComparator + s1=" + s1.price + " s2=" + s2.price);
			
			Date date1 = s1.getDate();
			Date date2 = s2.getDate();

			/**
			 * This generic Comparator will use the sort order of the tax lot
			 * method chosen.  Note that for ALL methods, the sort order 
			 * applies to the Opening OrderTicket as all Closing orders are FIFO
			 */
			
			switch( getTaxLotMethodId() ) {
			/**
			 * Descending order - Last In, First Out 
			 */ 
			case LIFO :		
				return date2.compareTo(date1);

			/** 
			 * Ascending order - First In, First Out 
			 */
			case FIFO :		
				return date1.compareTo(date2);
				
			case CLOSING_ORDER :
				return date1.compareTo(date2);
				
			case HIGH_COST :
				Double price1 = s1.getPrice();
				Double price2 = s2.getPrice();
				return price2.compareTo(price1);
				
			case LONG_TERM_HIGH_COST :  // Single sort method unlikely to solve problem....
				Double price3 = s1.getPrice();
				Double price4 = s2.getPrice();
				return price4.compareTo(price3);
				
			default:
				break;
			}
			return 0;
		}
	};

	public static Comparator<Transaction> TaxLotClosingComparator = new Comparator<Transaction>() {

		public int compare(Transaction s1, Transaction s2) {
//			System.out.println("STUB:: TaxLotClosingComparator + s1=" + s1.getPrice() + " s2=" + s2.getPrice());

			Date date1 = s1.getDate();
			Date date2 = s2.getDate();

			/** 
			 * Ascending order - First In, First Out 
			 */
			return date1.compareTo(date2);
		}
	};
}
