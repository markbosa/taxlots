package orders;

import java.util.Iterator;
import java.util.LinkedList;

import persistence.DaoClosingOrder;
import persistence.MongoDBUtils;
import calcEngine.EnumTaxLotMethod;
import calcEngine.TaxLot;
import calcEngine.TaxLotBucket;


/**
 * The requirements document "TaxLotScenarios.xls" describes a relationship
 * between an ClosingOrderTicket and Transactions, which could be CLOSE and CLOSE types.
 * 
 * The Parent OrderTicket will manage the Closing transactions in the 
 * ClosingOrderTicket, along with the Opening transactions in the OpeningOrderTicket
 * 
 * @author Mark
 *
 */
public class ClosingOrderTicket extends OrderTicket {
	private ClosingOrder closingOrder = null;
	private LinkedList<ClosingOrder> closeList = new LinkedList<ClosingOrder>();
	TaxLot currentTaxLot = null;
	TaxLotBucket taxLotBucket = new TaxLotBucket();
//	private int taxLotId = 1;

	/**
	 * 
	 */
	public ClosingOrderTicket(EnumTaxLotMethod txLtMethodId, String ticket) {
		super(ticket, txLtMethodId);
	}


	/**
	 * Not sure if we need to persist closing orders.....
	 * 
	 * @param mDbu
	 * @param closingOrderTicket
	 */
	public void closingOrderEtl(MongoDBUtils mDbu,
								ClosingOrderTicket closingOrderTicket) {
		/**
		 *  Perform ETL on Closing Orders 
		 */
		DaoClosingOrder dco = new DaoClosingOrder(closingOrderTicket, mDbu);
		
		Iterator<ClosingOrder>  closeIt = closeList.iterator();	
		
		while(closeIt.hasNext()) {

			ClosingOrder tliClose = closeIt.next();
				
			/**
			 * Now insert the closing orders one at a time
			 */
			dco.insertClosingOrder(tliClose);
		}		
	}


	/**
	 * Getter and Setter methods for ClosingOrderTicket
	 */
	public ClosingOrder getClosingOrder() {
		return closingOrder;
	}

	public void setClosingOrder(ClosingOrder closingOrd) {
		this.closingOrder = closingOrd;
	}

	public LinkedList<ClosingOrder> getClosingOrderList() {
		return closeList;
	}

	public void setClosingOrderList(LinkedList<ClosingOrder> closeList) {
		this.closeList = closeList;
	}

	public void closingOrderListAddClose(ClosingOrder closingOrd) {
		this.closeList.add(closingOrd);
	}
}


