package orders;

import java.util.LinkedList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Open OrderTicket extends Transaction by adding a list of the Close OrderTicket 
 * sub-executions that were executed on the Open OrderTicket.  
 * @author Mark Bosakowski
 *
 */
public class OpeningOrder extends Transaction {
	private LinkedList<ClosingOrder> sol = new LinkedList<ClosingOrder>();

	public OpeningOrder() {
	}

	public OpeningOrder(boolean openClose, String ordId, String tradeId, String corpActionType,
			int qnt, String sym, Date date,
			double prc, double stRate, double ltRate) {
		super(openClose, ordId, tradeId, corpActionType, qnt, sym, date, prc, stRate, ltRate);
	}

	public OpeningOrder(boolean openClose, String ordId, String tradeId, String corpActionType,
			int quantity, String sym, Date date, double price) {
		super(openClose, ordId, tradeId, corpActionType, quantity, sym, date, price, 0.0, 0.0);
	}

	public LinkedList<ClosingOrder> getSol() {
		return sol;
	}

	public void setSol(LinkedList<ClosingOrder> sol) {
		this.sol = sol;
	}
	
	/**
	 * For each full or partial ClosingOrder against THIS Open, we must keep a list
	 * of the CloseOrders that were used on this OPEN.  There may be several 
	 * CloseOrders per OpeningOrder closeOrd keep a list.
	 * 
	 * @param subEx  - A full close order (opens 100% of available shares)
	 *                   - A partial close order opens < 100% 
	 */
	public void addCloseToOpen(ClosingOrder subEx) {
		this.sol.add(subEx);
	}

	/**
	 * Each class that extends Transaction MUST implement a method that
	 * will print its own specific data
	 */
	@Override
	public void printSubData(boolean details) {
		Iterator<ClosingOrder>  solIt = ((List<ClosingOrder>) this.sol).iterator();	

		if(solIt.hasNext() == false)
			return;
		
		while(solIt.hasNext()) {
			ClosingOrder so = solIt.next();
			System.out.println("\tOpen  OrderTicket: " + this.getTradeId() +
					" fulfilled by Close OrderTicket: " + so.getTradeId()
					);
			
			if(details == true)
				so.printSubExecutions();
		}
//		System.out.println("\n");
	}

	public Object getPreviousDayPosition() {
		// TODO Auto-generated method stub
		return null;
	}


}
