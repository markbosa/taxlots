/**
 * 
 */
package calcEngine;


import java.util.ArrayList;
import java.util.Date;

import org.bson.types.ObjectId;

import orders.OpeningOrder;
import orders.OpeningOrderTicket;
import orders.ClosingOrder;
import orders.ClosingOrderTicket;
import orders.Transaction;
import orders.Transaction.TaxLotStatus;

/**
 * @author Mark
 *
 *	We have to decouple tax lots from transactions. A Tax Lot can be modeled as
 * 	an opening transaction and several closing transactions. Therefore we need 
 * 	two classes, one for TaxLot and one for Transaction. We need to be able to 
 * 	report which Transaction opens a Tax Lot and which close it and the G/L 
 * 	at the tax lot level.
 * 
 *	The big difference between the Tax Lots Methods is perhaps the way to select
 *	the open tax lot to be closed by a newly received transaction. 
 *	
 *	Therefore we could have a single method template and the selection step 
 *	being abstracted and implemented according to the specific tax lot method
 *	logic.
 *
 *  Either in this class or in other classes related to this class, we need to 
 *  report on these variables:
 *  
 *  Lot ID (just some unique Lot identifier)
 *  Account
 *  Security Name, ID, Type, Market
 *  Lot Date (when the Lot was opened)
 *  LT/ST (Long-Term/Short-Term)
 *  Lot Original Quantity
 *  Lot Original Cost
 *  Lot Unit Cost
 *  Previous Day Position, Today Position (referring to open quantity within the Lot)
 *  Bought (equal to Lot Original Qty if the Lot was opened today, zero on other days)
 *  Sold (Qty closed today)
 *  Price, Market Value (referring to the MV of the open position within the Lot)
 *  Realized G/L
 *  Unrealized G/L
 *
 */
public class TaxLot {
	private OpeningOrder openingOrder = null;
	private ClosingOrder currentClosingOrder = null;
	protected ArrayList<ClosingOrder> closingOrders = new ArrayList<ClosingOrder>();
	private OpeningOrderTicket  openingTicket = null;
	private ClosingOrderTicket closingTicket = null;
	private ObjectId id = null;
	
	private TaxLotStatus status = null;	// Must be set CLOSE or CLOSED Explicitly
	private int bitMask = 0;
	private int rank = 0;
	
	private int lotId = 0;
	private Date lotDate = new Date();	// Date when lot was opened i.e NOW!
	private String account = "";
	private String securityId = "";
	private int   lotOriginalQty = 0;
	private double lotOriginalCost = 0.0;
	private double lotUnitCost = 0.0;
	private double price = 0.0; 
	private double taxLotAmount = 0.0;
	private double marketValue = 0.0;
	private int boughtQty = 0;
	//private int soldQty = 0;
	private int todayPosition = 0;
	private int previousDayPosition = 0;
	private double realizedGain = 0.0;
	private double unrealizedGain = 0.0;
	/**
	 * Open orders will be used to open a tax lot. At the time of construction 
	 * only have a open, closeOrd set the opening transactions with understanding
	 * that if it takes multiple closing transactions, then each subsequent 
	 * closing transaction will update the closingTicket value. 
	 * @param tliOpen OpeningOrder
	 * @param clossingOrderTicket ClosingOrder
	 */
	public TaxLot(OpeningOrderTicket openingOrderTicket, OpeningOrder tliOpen) {
		this.setOpeningTicket(openingOrderTicket);
		this.setOpenOrder(tliOpen);
		this.setAccount(tliOpen.getAccountId());
		this.setBoughtQty(tliOpen.getQuantity());
		this.setLotOriginalQty(tliOpen.getQuantity());
		this.setLotOriginalCost(tliOpen.getQuantity() * tliOpen.getPrice());
		this.setLotUnitCost(this.lotOriginalCost / this.lotOriginalQty);
//		this.setSecurityId(tliOpen);   AssetType ???
		// TradeText
		this.setStatus(Transaction.TaxLotStatus.LOT_OPEN);
	}
	
	/**
	 * At the time of construction we already have paired a open and close
	 * items, closeOrd set the opening and closing transactions with understanding
	 * that if it takes multiple closing transactions, then each subsequent 
	 * closing transaction will update the closingTicket value. 
	 * @param openingOrderTicket OpeningOrder
	 * @param closingOrderTicket ClosingOrder
	 */
	public TaxLot(OpeningOrderTicket openingOrderTicket, ClosingOrderTicket closingOrderTicket) {
		this.setOpeningTicket(openingOrderTicket);
		this.setClosingTicket(closingOrderTicket);
		this.setStatus(Transaction.TaxLotStatus.LOT_OPEN);
	}
	public void setTx(ArrayList<ClosingOrder> tx) {
		this.closingOrders = tx;
	}
	
	
	/**
	 * For debugging - The drill down algorithm will be used to persist the
	 * tax lot and related information (references to transactions) to a 
	 * database 
	 */
	public void printTaxLot() {
              		System.out.printf( "Tax Lot ID: [%s]  Status: %s BitMask: %d Rank: %d\n"
				,this.lotId, this.status.name(), this.bitMask, this.rank );

        		System.out.printf( "  lotOriginalQty: %d lotDate: %s  \n",
				this.lotOriginalQty, this.getLotDate().toString());
		  		
        		/**
        		 * FIXXXXXX - Replace getOpeningTicket with a list iterator of bucket
        		 * info that is drilled down from TaxLot->TL1 --->> TLN, NOT Opening which 
        		 * will repeat the first transaction. 
        		 * 
        		 * We need to maintain the Opening Ticket (O1) which should maintain a list
        		 * of Transactions OPEN or CLOSE.  
        		 */
        		openingTicket.printOpenOrder(this.getOpenOrder());		
	}
	
	
	/**
	 * Getter / Setter methods
	 */
	public int getBitMask() {
		return bitMask;
	}

	public void setBitMask(int bitMask) {
		this.bitMask = bitMask;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getLotId() {
		return lotId;
	}

	public void setLotId(int taxLotId) {
		this.lotId = taxLotId;
	}
	public TaxLotStatus getStatus() {
		return status;
	}
	public void setStatus(TaxLotStatus status) {
		this.status = status;
	}
	public int getLotOriginalQty() {
		return lotOriginalQty;
	}
	public void setLotOriginalQty(int lotOriginalQty) {
		this.lotOriginalQty = lotOriginalQty;
	}
	public ClosingOrderTicket getClosingTicket() {
		return closingTicket;
	}
	public void setClosingTicket(ClosingOrderTicket closingTicket) {
		this.closingTicket = closingTicket;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public OpeningOrderTicket getOpeningTicket() {
		return openingTicket;
	}
	public void setOpeningTicket(OpeningOrderTicket openingOrderTicket) {
		this.openingTicket = openingOrderTicket;
	}
	public OpeningOrder getOpenOrder() {
		return openingOrder;
	}
	public void setOpenOrder(OpeningOrder openingOrder) {
		this.openingOrder = openingOrder;
	}

	public String getSecurityId() {
		return securityId;
	}

	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	public double getLotOriginalCost() {
		return lotOriginalCost;
	}

	public void setLotOriginalCost(double lotOriginalCost) {
		this.lotOriginalCost = lotOriginalCost;
	}

	public double getLotUnitCost() {
		return lotUnitCost;
	}

	public void setLotUnitCost(double lotUnitCost) {
		this.lotUnitCost = lotUnitCost;
	}

	public double getBoughtQty() {
		return boughtQty;
	}

	public void setBoughtQty(int boughtQty) {
		this.boughtQty = boughtQty;
	}

//	public int getSoldQty() {
//		return soldQty;
//	}
//
//	public void setSoldQty(int soldQty) {
//		this.soldQty = soldQty;
//	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTaxLotAmount() {
		return this.taxLotAmount;
	}
	
	public void sumTaxLotAmount(double newPrice) {
		this.setTaxLotAmount(this.getTaxLotAmount() + newPrice);
	}

	public void setTaxLotAmount(double taxAmount) {
		this.taxLotAmount = taxAmount;
	}

	public double getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(double marketValue) {
		this.marketValue = marketValue;
	}

	public Date getLotDate() {
		return lotDate;
	}

	public void setLotDate(Date lotDate) {
		this.lotDate = lotDate;
	}

	public int getPreviousDayPosition() {
		return previousDayPosition;
	}

	public void setPreviousDayPosition(int previousDayPosition) {
		this.previousDayPosition = previousDayPosition;
	}

	public int getTodayPosition() {
		return todayPosition;
	}

	public void setTodayPosition(int todayPosition) {
		this.todayPosition = todayPosition;
	}

	public double getUnrealizedGain() {
		return unrealizedGain;
	}

	public void setUnrealizedGain(double unrealizedGain) {
		this.unrealizedGain = unrealizedGain;
	}

	public double getRealizedGain() {
		return realizedGain;
	}

	public void setRealizedGain(double realizedGain) {
		this.realizedGain = realizedGain;
	}

	/**
	 * The Lot realized gain is the sum of the 1->N realized gains of the 
	 * close orders.
	 * @param realizedGain2
	 */	
	public void sumRealizedGain(double realizedGain2) {
		this.setRealizedGain(this.getRealizedGain() + realizedGain2);		
	}

	/**
	 * Unrealized gain is defined as:
	 * Let�s assume that today the XYZ price is $40/share.
	 * In the first example:
	 * 
	 * Unrealized PNL = (Current Price � Position Average Cost) * Position Shares = ($40 - $22.5) * 200 = $3,500
	 * Realized PNL = $200
	 * Total PNL = Realized PNL + Unrealized PNL = $3,500 + $200 = $3,700
	 * 
	 * @param open
	 * @param currentTaxLot 
	 */
	public void setUnrealizedGain(OpeningOrder open, ClosingOrder close) {
		
		this.unrealizedGain  = (close.getPrice() - open.getPrice()) * open.getBalance(); 
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ClosingOrder getCurrentClosingOrder() {
		return currentClosingOrder;
	}

	public void setCurrentClosingOrder(ClosingOrder currentClosingOrder) {
		this.currentClosingOrder = currentClosingOrder;
	}
}
