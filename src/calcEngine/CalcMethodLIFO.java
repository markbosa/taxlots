package calcEngine;

import java.util.Collections;

import orders.Transaction;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;

/**
 * @author Mark Bosakowski
 *
 * CalcMethodLIFO - Implements the LIFO Tax Lot Method
 * 
 * LIFO (Last In, First Out) Tax Lot Relief Method close the 
 * most recently acquired shares in the account first.
 */            
public class CalcMethodLIFO extends TaxLotMethod {

	public CalcMethodLIFO(SqlServerClient sqlClient, MongoDBUtils mongoClient) {
		super(EnumTaxLotMethod.LIFO, sqlClient, mongoClient);
	}

	@Override
	public double sortOrders() {
		// Verify input data first... remove after testing
		printTaxLotItems("LIFO:: BEFORE Sorting");

		/**
		 * Sort by descending date and open/closed flag
		 */
		Collections.sort(this.getOpen(),  Transaction.TaxLotOpeningComparator);
		Collections.sort(this.getClose(), Transaction.TaxLotClosingComparator);

		printTaxLotItems("LIFO:: AFTER Sorting.."); 

		return 0;
	}
}
