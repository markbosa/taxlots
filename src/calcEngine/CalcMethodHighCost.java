/**
 * 
 */
package calcEngine;

import java.util.Collections;

import orders.Transaction;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;

/**
 * @author Mark
 *
 */
public class CalcMethodHighCost extends TaxLotMethod {

	/**
	 * @param taxLotMethodIdentifier
	 */
	public CalcMethodHighCost(SqlServerClient sqlClient, MongoDBUtils mongoClient) {
		super(EnumTaxLotMethod.HIGH_COST, sqlClient, mongoClient);
	}

	@Override
	public double sortOrders() {
		// Verify input data first... remove after testing
		printTaxLotItems("HIGH_COST_TAX_LOT:: BEFORE Sorting");

		/**
		 * Sort by descending date and open/closed flag
		 */
		Collections.sort(this.getOpen(),  Transaction.TaxLotOpeningComparator);
		Collections.sort(this.getClose(), Transaction.TaxLotClosingComparator);

		printTaxLotItems("HIGH_COST_TAX_LOT:: AFTER Sorting");
		return 0;
	}
	
}
