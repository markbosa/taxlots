package calcEngine;

public enum EnumLongTermHighCost {
	SHORT_TERM_LOSS(8),
	LONG_TERM_LOSS(16),
	SHORT_TERM_ZERO(10),
	LONG_TERM_ZERO(18),
	LONG_TERM_GAIN(20),
	SHORT_TERM_GAIN(12);
	
	private int id;
	
	EnumLongTermHighCost(int id) {
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	/**
	 * Analysis will assign bit values (see top of page) and that value is 
	 * sent to this helper function to return a relative ranking that is used
	 * to specifically order the type of "closed to open" relationship.  Once we
	 * rank each open/closed pair, then we sort them ascending by rank, then by
	 * descending price value for Losses, ascending price value for Gains.   
	 * @param bitVal
	 * @return rank from 1 to 6
	 */
	public int getRank(int bitVal) {
		
		if(bitVal == SHORT_TERM_LOSS.id )
			return 1;
		else if(bitVal == LONG_TERM_LOSS.id ) 
			return 2;
		else if(bitVal == SHORT_TERM_ZERO.id ) 
			return 3;
		else if(bitVal == LONG_TERM_ZERO.id ) 
			return 4;
		else if(bitVal == LONG_TERM_GAIN.id ) 
			return 5;
		else if(bitVal == SHORT_TERM_LOSS.id ) 
			return 6;
		else
			return -1;
	}
}
