package calcEngine;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/**
 * TaxLotBucket - A container to store Opening and Closing Trades during the
 * process of selecting a Trade based on the Tax Lot Method sorting criteria.
 * 
 * Basic algorithm is to evaluate an Opening Trade vs. a Closing Trade, and 
 * based on the the following criteria, determine which bucket to add the the
 * trade to.  Once all trades have been evaluated, we can then sort the bucket
 * on other criteria, such as biggest loss, smallest gain, etc. 
 * 
 * @author Mark Bosakowski
 *
 */
public class TaxLotBucket {
	
	private EnumLongTermHighCost ltHcType = null;
	ArrayList<TaxLot> taxBucket = new ArrayList<TaxLot>();
	
	public TaxLotBucket() {
	}

	public TaxLotBucket(EnumLongTermHighCost type) {
		setLtHcType(type);
	}

	public void addTaxLot( TaxLot taxLot) {
		this.taxBucket.add(taxLot);
	}
	
	public void dumpTheBucket() {
		
		Iterator<TaxLot> tbIt = this.taxBucket.iterator();
		
		if(tbIt.hasNext() == false) {
			System.out.println("TaxLotBucket is empty - No tax lots were processed" );
		}
		else {
			int count = 1;
			while(tbIt.hasNext()) {
				TaxLot taxLot = tbIt.next();

				System.out.println("TaxLotBucket item[" + count++ +  "] : "
									+ taxLot.getLotId() );
				taxLot.printTaxLot();
			}
		}
	}

	/**
	 * Several Tax Lot Methods apply more than one "open orders" vs. one or more "close orders"
	 */
	public static Comparator<TaxLot> TaxLotClosingComparator = new Comparator<TaxLot>() {

		@Override
		public int compare(TaxLot t1, TaxLot t2) {
			Integer taxOne = new Integer(t1.getRank());
			Integer taxTwo = new Integer(t2.getRank());

			System.out.println("STUB:: TaxLotBucketComparator + t1=" + t1.getRank() + " t2=" + t2.getRank());

			/**
			 * Descending OrderTicket - Highest value on top
			 */
			return taxTwo.compareTo(taxOne);
		}
	};
	/**
	 * Getter and Setter methods
	 * @return
	 */
	public EnumLongTermHighCost getLtHcType() {
		return ltHcType;
	}

	public void setLtHcType(EnumLongTermHighCost ltHcType) {
		this.ltHcType = ltHcType;
	}
}
