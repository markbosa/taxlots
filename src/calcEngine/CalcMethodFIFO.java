package calcEngine;

import java.util.Collections;
import java.util.Iterator;

import orders.IntradayOrder;
import orders.ClosingOrder;
import orders.Transaction;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;

/**
 * @author Mark Bosakowski
 *
 * CalcMethodFIFO - Implements the FIFO Tax Lot Method

 * FIFO (First In, First Out) Tax Lot Relief Method closes the 
 * most oldest acquired shares in the account first.
 */
public class CalcMethodFIFO extends TaxLotMethod {

	public CalcMethodFIFO(SqlServerClient sqlClient, MongoDBUtils mongoClient) {
		super(EnumTaxLotMethod.FIFO, sqlClient, mongoClient);
	}	
	

	@Override
	public double sortOrders() {
		// Verify input data first... remove after testing
		printTaxLotItems("FIFO::" + ((this.getIntraday() == true) ? "Intraday" : "" ) +" BEFORE Sorting");

		/**
		 * Sort by descending date and open/closed flag
		 */
		Collections.sort(this.getOpen(),  Transaction.TaxLotOpeningComparator);
		Collections.sort(this.getClose(), Transaction.TaxLotClosingComparator);
		
		if(this.getIntraday() == true) {
			System.out.println("FIFO:: Intraday Option Enabled\n");
			
			boolean brc = validateCloseOrders();
			
			if(brc == false) {
				System.out.println("FIFO Intraday Validation failure - Request rejected");
				return -1;
			}
			
			IntradayOrder intradayOrder = new IntradayOrder(this.getOpen());
			
			intradayOrder.IntradaySort(this.getOpen(), this.getClose());
		}

		printTaxLotItems("FIFO::" + ((this.getIntraday() == true) ? "Intraday" : "" ) +" AFTER Sorting");
		return 0;
	}
	
	/**
	 * This method may or may not be needed, but to be on the safe side, this
	 * method will check that ALL of the close orders in the list have the 
	 * same "close date" as we are attempting to process an Intraday FIFO 
	 * Tax Lot Method calculation.
	 * 
	 * Once the interface to the Liquid Trade database is complete, this method
	 * should be reviewed to see if it is still needed.
	 *  
	 * @return true is all close dates are the same
	 */
	
	boolean validateCloseOrders() {
		/**
		 *  Outer loop only processes close orders
		 */
		Iterator<ClosingOrder> closeIt = this.getClose().iterator();
		int compare = 0;
		
		while(closeIt.hasNext()) {
			ClosingOrder close1 = closeIt.next();
			
			if(closeIt.hasNext()) {
				ClosingOrder close2 = closeIt.next();
				compare = close1.compareCloseDates(close2);
			}
			if( compare != 0) {
				System.out.println("FIFO Intraday Validation Failed: Different Close dates");
				return false;
			}
		}
		return true;	
	}
}
