/**
 * 
 */
package calcEngine;

/**
 * @author Mark Bosakowski
 *
 * Enumerate the 5 MUST-HAVE Tax Lot Methods
 * Architectural Note:  These ID's will be selectable items in user interface
 * which may include drop-downs, check box's etc.   The selection will be 
 * manifested as in index which will be used to invoke the appropriate tax lot
 * calculation code. 
 * 
 * Note that this list may be extended or may include a customizable tax lot
 * selection. 
 */
public enum EnumTaxLotMethod {
	NO_TAX_LOT_METHOD,
	FIFO,
	FIFO_INTRADAY,
	LIFO,
	HIGH_COST,
	LONG_TERM_HIGH_COST,
	BEST_TAX,
	OPENING_ORDER_ETL_ONLY,
	CLOSING_ORDER;
	
	public static EnumTaxLotMethod setMethodByString( String index) {
		
		if(index.compareTo( NO_TAX_LOT_METHOD.toString()) == 0)
				return EnumTaxLotMethod.NO_TAX_LOT_METHOD;
		else if(index.compareTo( FIFO.toString()) == 0)
				return EnumTaxLotMethod.FIFO;
		else if(index.compareTo( FIFO_INTRADAY.toString()) == 0)
			return EnumTaxLotMethod.FIFO_INTRADAY;	
		else if(index.compareTo( LIFO.toString()) == 0)
				return EnumTaxLotMethod.LIFO;
		else if(index.compareTo( HIGH_COST.toString()) == 0)
			return EnumTaxLotMethod.HIGH_COST;
		else if(index.compareTo( LONG_TERM_HIGH_COST.toString()) == 0)
			return EnumTaxLotMethod.LONG_TERM_HIGH_COST;
		else if(index.compareTo( BEST_TAX.toString()) == 0)
			return EnumTaxLotMethod.BEST_TAX;
		else if(index.compareTo( OPENING_ORDER_ETL_ONLY.toString()) == 0)
			return EnumTaxLotMethod.OPENING_ORDER_ETL_ONLY;
		else if(index.compareTo( CLOSING_ORDER.toString()) == 0)
			return EnumTaxLotMethod.CLOSING_ORDER;
		else {
			System.out.println(
					"EnumTaxLotMethod::setMethodByString() ERROR: Tax Method [" 
					+ index + "] did not match any standard method.  Exit now");
			System.exit(-99);
			return null;
		}
	}
}
