package calcEngine;

/**
 * Tax Rate Enumeration
 * 
 * Constants mapped to double incomeRates[]
 *      = { 0.10, 0.15, 0.25, 0.28, 0.33, 0.396 };
 * @author Mark
 *
 */
public enum EnumTaxRate {
	TAX_RATE_10,
	TAX_RATE_15,
	TAX_RATE_25,
	TAX_RATE_28,
	TAX_RATE_33,
	TAX_RATE_396
}
