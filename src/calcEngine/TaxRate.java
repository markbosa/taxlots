package calcEngine;

import java.util.ArrayList;
import java.util.Iterator;

public class TaxRate {
	private ArrayList<taxRateTier> taxRate = new ArrayList<taxRateTier>();
	public final int TAX_TIERS = 6;
	private double capGainRate = 0.0;		// Income bracket 
	private double shortTermRate = 0.0;
	private double longTermRate = 0.0;
	double shortTermTaxRate[]= { 0.10, 0.15, 0.25, 0.28, 0.33, 0.396 };
	double longTermTaxRate[] = { 0.00, 0.00, 0.15, 0.15, 0.15, 0.200 };
	/**
	 * Populate the myTaxRate ArrayList with Federal Income Tax rates for 2014
	 */
	public TaxRate() {

		for(int i = 0; i < TAX_TIERS; i++) {
			getTaxRate().add(new taxRateTier( shortTermTaxRate[i], longTermTaxRate[i]));
		}
	}
	
	/**
	 * For any income rate, calculate the corresponding Capital Gain Rate.
	 * 
	 * The algorithm allows for any income rate and will return the correct
	 * income bracket rate. 
	 * 
	 * @param incomeRate - The taxable income rate
	 * @return nothing, but sets values of short and long term rates
	 */
	public void setCapitalGainRates(double incomeRate)
	{
		int index = 0;
		capGainRate = incomeRate;

		while(index < TAX_TIERS) {
			if( getTaxRate().get(index++).getIncomeRate() >= incomeRate )
				break;
		}
		setShortTermRate(shortTermTaxRate[index -1]);
		setLongTermRate(longTermTaxRate[index -1]);
	}

	public double getShortTermRate() {
		return shortTermRate;
	}

	public void setShortTermRate(double shortTermRate) {
		this.shortTermRate = shortTermRate;
	}

	public double getLongTermRate() {
		return longTermRate;
	}

	public void setLongTermRate(double longTermRate) {
		this.longTermRate = longTermRate;
	}

	public void printTaxRateTable() {
		Iterator<taxRateTier> trit = this.getTaxRate().iterator();
		taxRateTier tier = new taxRateTier();

		System.out.println("\n=========== Capital Gain 2014 Tax Rate Table =======================");

		while(trit.hasNext()) {
			tier = (taxRateTier) trit.next();

			System.out.printf(	
					" Short Term Rate: %.3f%%    Long Term Rate: %.2f%%%n", 
					tier.getIncomeRate(), tier.capitalGainRate);
		}
		System.out.println("\n================== Account Tax Rate ==============================");

		this.printMyTaxRate();
		System.out.println("==================================================================\n");
	}
	
	
	public void printMyTaxRate() {
		System.out.printf("Income Tier    : %5.3f%%%nShort Term Rate: %5.3f%%%nLong Term Rate : %5.3f%%%n",
				this.capGainRate, this.shortTermRate, this.longTermRate);
	}
	
	/**
	 * taxRateTier - A sub-class that allows us to encapsulate tier information
	 * @author Mark
	 * 
	 * Note that this may be expanded in the future to allow for differing tax
	 * rates in different years
	 */
	public class taxRateTier {
		private double incomeRate = 0.0;
		double capitalGainRate = 0.0;
		
		public taxRateTier() {
		}
		
		public taxRateTier( double income, double capGains) {
			this.setIncomeRate(income);
			this.capitalGainRate = capGains;
		}

		public double getIncomeRate() {
			return incomeRate;
		}

		public void setIncomeRate(double incomeRate) {
			this.incomeRate = incomeRate;
		}
	}
	
	public static void main(String[] args) {
		TaxRate tr = new TaxRate();
		tr.printTaxRateTable();
	}

	public ArrayList<taxRateTier> getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(ArrayList<taxRateTier> taxRate) {
		this.taxRate = taxRate;
	}

}
