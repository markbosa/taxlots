/**
 * 
 */
package calcEngine;

import java.util.Collections;
import java.util.Iterator;

import orders.OpeningOrder;
import orders.ClosingOrder;
import orders.SubExecution;
import orders.Transaction;
import persistence.MongoDBUtils;
import persistence.SqlServerClient;

/**
 * @author Mark
 * _____________________________________________________________________________________________________________
 * 						 Bit Mapping	Loss	Zero G/L	Gain	Short-Term	Long-Term	Integer Value	RANK
 * 							Bit number	1		2			4		8			16			Bitwise OR	
 * _____________________________________________________________________________________________________________
 * Close Short-Term Losses � Descending	1							8						9				1
 * Close Long-Term Losses  � Descending	1										16			17				2
 * Close Short-Term Zero G/L	 				2					8						10				3
 * Close Long-Term Zero G/L						2								16			18				4	
 * Close Long-Term Gains  � Ascending						4					16			20				5
 * Close Short-Term Gains � Ascending						4		8	 					12				6
 * 
 * Using the bit mapping above, we process each Closing OrderTicket vs. all Open Orders.   The processing will compare the 
 * open and close costs and determine if it was a GAIN, LOSS or Zero, and what the Short OR Long Term taxes would
 * be for that Open / Closing pair.
 * 
 *  All Open Orders will extend their record to include the Integer Bitwise value which through a lookup table will
 *  give us the RANK of the Opening Orders to process.  Once we have evaluated each open order, we can then sort them
 *  by RANK and by the size of the GAIN or LOSS.  We will determine whether it is short or long term based upon AFTER
 *  the SORT as we cannot determine which Closing OrderTicket will be paired with the Opening Orders. 
 */
public class CalcMethodLongTermHighCost extends TaxLotMethod {

	/**
	 * @param taxLotMethodIdentifier
	 */
	public CalcMethodLongTermHighCost(SqlServerClient sqlClient, MongoDBUtils mongoClient) {
		super(EnumTaxLotMethod.LONG_TERM_HIGH_COST, sqlClient, mongoClient );
	}

	/** 
	 * @see calcEngine.TaxLotMethod#sortOrders()
	 */
	@Override
	public double sortOrders() {
		// Verify input data first... remove after testing
		printTaxLotItems("LongTermHighCost:: BEFORE Sorting");
		
		/**
		 * Sort by descending date and open/close flag
		 */
		Collections.sort(this.getOpen(),  Transaction.TaxLotOpeningComparator);
		Collections.sort(this.getClose(), Transaction.TaxLotClosingComparator);

		sortByLongTermHighCost();
		
		printTaxLotItems("LongTermHighCost:: AFTER Sorting"); 
		return 0;
	}

	/**
	 * XXXXXXXXXXXXXX Cut from TaxLotMethod... NEEDS REWRITE!
	 */
	public void sortByLongTermHighCost(){
		/**
		 * This is complex algorithm that will RANK close / open order pairs
		 * by generating TaxLot objects that will be added to a Tax Lot Bucket
		 * The bucket will be sorted by RANK first, then by price
		 */
		/**
		 *  Outer loop only processes close orders
		 */
		Iterator<ClosingOrder> closeIt = getClose().iterator();

		/**
		 * The algorithm will process one CLOSE order at a time which is
		 * the outer loop.  For each CLOSE order, we liquidate the OPEN in a 
		 * the pre-sorted order.
		 */
		while(closeIt.hasNext()) {
			ClosingOrder tliClose = closeIt.next();

			/**
			 * In the outer loop, process CLOSE orders
			 *
			 * Now execute CLOSE orders vs. CLOSES in FIFO or LIFO sorted order
			 */
			int closeQuantity = tliClose.getQuantity();
			boolean fulfilled = false;

			/**
			 *  Inner loop - Calculate Tax on OPEN Orders by Date < or > 1 year
			 */
			Iterator<OpeningOrder>  openIt = getOpen().iterator();	

			while(openIt.hasNext() && (fulfilled == false)) {

				OpeningOrder tliOpen = openIt.next();

				/**
				 * If item has been processed (CLOSED) then we skip it
				 * We MUST consider only Open or Partially Closed items
				 */
				if(tliOpen.status == Transaction.TaxLotStatus.LOT_ITEM_CLOSED)
					continue;

				/**
				 * For close orders that require more than one OPEN Tax Lot item,
				 * we must keep a running total of how much we have sold and 
				 * how much left to fulfill in the close order.
				 * 
				 * Note that sliClose.balance is initially zero meaning that 
				 * nothing has yet been sold.
				 */
				closeQuantity = tliClose.getQuantity() - tliClose.getBalance();

				int openQuantity = 0;

				if(tliOpen.status == Transaction.TaxLotStatus.LOT_ITEM_PARTIAL_CLOSE)
					openQuantity =  tliOpen.getBalance();
				else
					openQuantity =  tliOpen.getQuantity();

				long bsDiff = closeQuantity - openQuantity; 

				/**
				 * We need to determine one of three conditions
				 * 1) An Exact Match - Open 100 - Close 100 shares
				 * 2) A subset is sold Open 100 - Close 50 shares
				 * 3) An insufficient quantity in OPEN order which will 
				 * 	  require another OPEN order in the lot in order to 
				 *    fulfill the CLOSE order.  CLOSE 200 from 2 100 OPEN 
				 *    
				 *    The "fulfilled" boolean will flag when the Open Trade
				 *    order has been satisfied and will trigger the exit from 
				 *    the inner loop for THAT Close Trade order only.
				 */
				if(bsDiff == 0) {
					/**
					 * Calculate Tax based on date
					 */
					System.out.println(this.getMethodName() +
							" CLOSE: " + closeQuantity + 
							" > OPEN: " + openQuantity  + " at: " + 
							tliOpen.getPrice());

					tliOpen.status = Transaction.TaxLotStatus.LOT_ITEM_CLOSED;
					tliOpen.setBalance(tliOpen.getBalance() - closeQuantity);

					tliClose.status = Transaction.TaxLotStatus.LOT_ITEM_CLOSED;
					tliClose.setBalance(tliClose.getBalance() + closeQuantity);

					tliClose.calculateRealizedGain(tliOpen, tliClose);
					double itemTax = tliOpen.calculateTaxAmount(tliOpen, tliClose);
					System.out.println("itemTax = " + itemTax);

					/**
					 * For cross tab validation sum amount in TaxLot container
					 */
//					this.currentTaxLot.sumTaxLotAmount(itemTax);
					
					SubExecution subEx = new SubExecution(	
							openQuantity, 
							closeQuantity,
							tliClose.getTaxAmount(),
							tliClose.getTaxRateString());
					
					tliClose.addToCloseOrder(subEx);
					tliOpen.addCloseToOpen(tliClose);
					
					fulfilled = true;
				}
				else if( bsDiff < 0)   {
					System.out.println(this.getMethodName() +
							" CLOSE: " + closeQuantity + 
							" > OPEN: " + openQuantity  + " at: " + 
							tliOpen.getPrice());

					tliOpen.status = Transaction.TaxLotStatus.LOT_ITEM_PARTIAL_CLOSE;
					tliOpen.setBalance(tliOpen.getBalance() - closeQuantity);

					tliClose.status = Transaction.TaxLotStatus.LOT_ITEM_CLOSED;
					tliClose.setBalance(tliClose.getBalance() + closeQuantity);

					tliClose.calculateRealizedGain(tliOpen, tliClose);
					double itemTax = tliOpen.calculateTaxAmount(tliOpen, tliClose);
					System.out.println("itemTax = " + itemTax);
					System.out.println("itemTax = " + itemTax);

					/**
					 * For cross tab validation sum amount in TaxLot container
					 */
//					this.currentTaxLot.sumTaxLotAmount(itemTax);

					SubExecution subEx = new SubExecution(	
							openQuantity, 
							closeQuantity,
							tliClose.getTaxAmount(),
							tliClose.getTaxRateString());
					
					tliClose.addToCloseOrder(subEx);
					tliOpen.addCloseToOpen(tliClose);
					fulfilled = true;
				}
				else if( bsDiff > 0) {
					System.out.println(this.getMethodName() +
							" CLOSE: " + closeQuantity + 
							" > OPEN: " + openQuantity  + " at: " + 
							tliOpen.getPrice());

					tliOpen.status = Transaction.TaxLotStatus.LOT_ITEM_CLOSED;
					tliOpen.setBalance(tliOpen.getBalance() - openQuantity);

					tliClose.status = Transaction.TaxLotStatus.LOT_ITEM_PARTIAL_CLOSE;
					tliClose.setBalance(tliClose.getBalance() + openQuantity);

					tliClose.calculateRealizedGain(tliOpen, tliClose);
					double itemTax = tliOpen.calculateTaxAmount(tliOpen, tliClose);
					System.out.println("itemTax = " + itemTax);

// XXXX Needs to re-written to model TaxLotMethod which has had a LOT of changes...					
					/**
					 * For cross tab validation sum amount in TaxLot container
					 */
//					this.currentTaxLot.sumTaxLotAmount(itemTax);
					
					/**
					 * For cross tab validation - save amount in the 
					 * 1) the Close OrderTicket and 2) sum in TaxLot container
					 */
					
					SubExecution subEx = new SubExecution(	
							openQuantity, 
							closeQuantity,
							tliClose.getTaxAmount(),
							tliClose.getTaxRateString());
					
					tliClose.addToCloseOrder(subEx);
					tliOpen.addCloseToOpen(tliClose);
					continue;
				}
			}			
		}
	}	
}